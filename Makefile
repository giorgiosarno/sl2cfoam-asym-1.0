QUIET ?= @

CC = gcc-8
AR = ar
RLIB = ranlib

CFLAGS = -O2 -std=c99 -fopenmp -fPIC -g -Iwigxjpf-1.7/inc/ -Iinc/ 
LDFLAGS = -Lwigxjpf-1.7/lib/ 
LDLIBS =  -fopenmp -lgsl -lgslcblas -lconfig -lmpfr -lmpc -lgmp -lwigxjpf -lm

SRCDIR = src
OBJDIR = obj
INCDIR = inc
LIBDIR = lib
BINDIR = bin

default: amplitude
all: default

INCS = inc/B4Function.h inc/Utilities.h inc/JSymbols.h  inc/khash.h inc/CoherentStatesFunctions.h inc/Common.h
_OBJS = B4Function.o Utilities.o khash.o CoherentStatesFunctions.o JSymbols.o Common.o
OBJS = $(patsubst %,$(OBJDIR)/%,$(_OBJS))

# library/inc object files
$(OBJDIR)/%.o: $(INCDIR)/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $<

# hash/inc object files
$(OBJDIR)/%.o: $(INCDIR)/%.h $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $< 

# src binaries object files
$(OBJDIR)/%.o: src/%.c $(INCS)
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c -o $@ $< -I$(INCDIR)/

# Built Library
$(LIBDIR)/libSL2C.a: $(OBJS)
	@echo "   AR    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(AR) $(ARFLAGS) $@ $(OBJS)
	$(QUIET)$(RLIB) $@

# Compile Main Programs
$(BINDIR)/CoherentStates: $(LIBDIR)/libSL2C.a $(OBJDIR)/CoherentStates.o 
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/CoherentStates.o $(LDFLAGS) -Llib/ -lSL2C $(LDLIBS) -lSL2C -o $@

$(BINDIR)/B4Hash: $(LIBDIR)/libSL2C.a $(OBJDIR)/B4Hash.o  
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/B4Hash.o $(LDFLAGS) -Llib/ -lSL2C $(LDLIBS) -lSL2C -o $@

$(BINDIR)/J6Hash: $(LIBDIR)/libSL2C.a $(OBJDIR)/J6Hash.o 
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/J6Hash.o $(LDFLAGS) -Llib/ -lSL2C $(LDLIBS) -lSL2C -o $@

$(BINDIR)/SL2CSymbol: $(LIBDIR)/libSL2C.a $(OBJDIR)/SL2CSymbol.o 
	@echo "   CC    $@"
	$(QUIET)mkdir -p $(dir $@)
	$(QUIET)$(CC) $(OBJDIR)/SL2CSymbol.o $(LDFLAGS) -Llib/ -lSL2C $(LDLIBS) -lSL2C -o $@

.PHONY: default all clean

lib: $(LIBDIR)/libSL2C.a
amplitude: $(BINDIR)/CoherentStates $(BINDIR)/B4Hash $(BINDIR)/J6Hash $(BINDIR)/SL2CSymbol
sl2c: $(BINDIR)/SL2CSymbol

clean: 
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)
	rm -rf $(BINDIR)