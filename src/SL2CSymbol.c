// gcc-7 -std=c99 -fopenmp -lgmp -lmpfr -lmpc -Lwigxjpf-1.5/lib -Iwigxjpf-1.5/inc -Iinc src/SL2CSymbol.c -lm -lwigxjpf -lconfig -o bin/SL2CSymbol

#include "Common.h"
#include "JSymbols.h"                 //Canonicalization of 6j symbols
#include "khash.h"                    //Hash Table
#include "Utilities.h"                //Various Functions
#include "CoherentStatesFunctions.h"

// To enable K summation cutoff
#define BOOSTERS_K_CUTOFF 0
// To enable data saving
#define SAVE_DATA 1
// Simmetric = 1, all boosted tetrahedra have equal set of boundary spins
#define SIMMETRIC 0
// Hash = 1, cache friendly and fast booster arrays. It doesn't work
// with many shells
#define CACHE 0

#if CACHE
// Number of B4 indices
static double B4[B4_CELLS_L][B4_CELLS_INT][B4_CELLS_INT] = {0};
// Number of Coherent indices
#define COH_CELLS 1000
static double complex CoherentStates[6][COH_CELLS];
#endif

int main(int argc, char **argv){

  //////////////////// Initialize configuration file ////////////////////

  config_t cfg, *cf;
  const config_setting_t *Two_Spins;
  const config_setting_t *SumDihedralAngles;
  const config_setting_t *ErrorPer;
  const config_setting_t *ImmirziPar;
  const config_setting_t *Config;

  unsigned int  two_j1Min, two_j2Min, two_j3Min, two_j4Min, two_j5Min,
                two_j6Min, two_j7Min, two_j8Min, two_j9Min, two_j10Min;

  cf = &cfg;
  config_init(cf);

  //////////////////// Check configuration file ////////////////////

  if (!config_read_file(cf, "../config/SL2Configurations.txt")){

      fprintf(stderr, "%s:%d - %s\n",
              config_error_file(cf),
              config_error_line(cf),
              config_error_text(cf));
      config_destroy(cf);
      return(EXIT_FAILURE);
  }

  //////////////////// Initialize minimum boundary spins ////////////////////

  char configTwo_Spins[200];
  sprintf(configTwo_Spins, "%s.Two_Spins",argv[1]);

  Two_Spins = config_lookup(cf, configTwo_Spins);
  two_j1Min = config_setting_get_int_elem(Two_Spins, 0);
  two_j2Min = config_setting_get_int_elem(Two_Spins, 1);
  two_j3Min = config_setting_get_int_elem(Two_Spins, 2);
  two_j4Min = config_setting_get_int_elem(Two_Spins, 3);
  two_j5Min = config_setting_get_int_elem(Two_Spins, 4);
  two_j6Min = config_setting_get_int_elem(Two_Spins, 5);
  two_j7Min = config_setting_get_int_elem(Two_Spins, 6);
  two_j8Min = config_setting_get_int_elem(Two_Spins, 7);
  two_j9Min = config_setting_get_int_elem(Two_Spins, 8);
  two_j10Min = config_setting_get_int_elem(Two_Spins, 9);

  //////////////////// Initialize Immirzi parameter ////////////////////

  char configImmirzi[200];
  sprintf(configImmirzi, "%s.Immirzi",argv[1]);
  ImmirziPar = config_lookup(cf, configImmirzi);
  float Immirzi = config_setting_get_float(ImmirziPar);
  float ImmirziSave =trunc(100 * Immirzi) / 100;

  //////////////////// Initialize Configuration type:
  //////////////////// Euclidean or Lorentzian

  char configConfig[200];
  sprintf(configConfig, "%s.Config",argv[1]);
  Config = config_lookup(cf, configConfig);
  int config_type = config_setting_get_int(Config);

  //////////////// Initialize Cut off for boundary gaussian states////////////////

  unsigned int cutoff_gaussian = atoi(argv[4]);

  //////////////////// Check data folders ////////////////////

  char folderPath[200];
  char folderPath1[200];
  char folderPath2[200];
  char folderPath3[200];

  struct stat st = {0};

  sprintf(folderPath, "../data/Immirzi_%.2f",ImmirziSave);
  sprintf(folderPath1, "../data/Immirzi_%.2f/HashTablesBooster",ImmirziSave);
  sprintf(folderPath2, "../data/Immirzi_%.2f/SL2CResults",ImmirziSave);
  sprintf(folderPath3, "../data/Immirzi_%.2f/HashTablesSL2C",ImmirziSave);

  if (stat(folderPath, &st) == -1) {

      mkdir(folderPath, 0700);
      mkdir(folderPath1, 0700);
      mkdir(folderPath2, 0700);
      mkdir(folderPath3, 0700);

  }

  if (stat(folderPath1, &st) == -1) {
      mkdir(folderPath1, 0700);
  }
  if (stat(folderPath2, &st) == -1) {
      mkdir(folderPath2, 0700);
      mkdir(folderPath3, 0700);
  }

  char folderPath4[200];
  sprintf(folderPath4,"../data/Immirzi_%.2f/SL2CResults/CutOff_%i",ImmirziSave,cutoff_gaussian);

  if (stat(folderPath4, &st) == -1) {

      mkdir(folderPath4, 0700);
  }

  char folderPath5[200];
  sprintf(folderPath5,"../data/Immirzi_%.2f/HashTablesSL2C/CutOff_%i",ImmirziSave,cutoff_gaussian);

  if (stat(folderPath5, &st) == -1) {

      mkdir(folderPath5, 0700);
  }

  //////////////////// Rescaling parameters and maximal Dl ////////////////////

  unsigned int two_N = atoi(argv[2]);
  unsigned int two_Dl = atoi(argv[3]);

  unsigned int  two_j1, two_j2, two_j3, two_j4, two_j5,
                two_j6, two_j7, two_j8, two_j9, two_j10;

  two_j1 = two_N*two_j1Min; two_j2 = two_N*two_j2Min; two_j3 = two_N*two_j3Min;
  two_j4 = two_N*two_j4Min; two_j5 = two_N*two_j5Min; two_j6 = two_N*two_j6Min;
  two_j7 = two_N*two_j7Min; two_j8 = two_N*two_j8Min; two_j9 = two_N*two_j9Min;
  two_j10 = two_N*two_j10Min;

  char pathRead1[200];
  sprintf(pathRead1,"../data/Immirzi_%.2f/SL2CResults/CutOff_%i/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.txt",
          ImmirziSave, cutoff_gaussian, two_j1, two_j2, two_j3, two_j4, two_j5,
          two_j6, two_j7, two_j8, two_j9, two_j10,two_Dl);

  //////////////////// Check if results already exist ////////////////////

  if (file_exist (pathRead1) == 0 ){

    unsigned int  two_limi1_1, two_limi1_2,two_limi2_1, two_limi2_2,two_limi3_1,
                  two_limi3_2,two_limi4_1, two_limi4_2,two_limi5_1, two_limi5_2;

    //////////////////// Range for boundary intertwiners ////////////////////

    two_limi1_1 = max(abs(two_j2-two_j3),abs(two_j5-two_j4));
    two_limi1_2 = min(two_j2+two_j3,two_j5+two_j4);
    two_limi2_1 = max(abs(two_j10-two_j7),abs(two_j1-two_j2));
    two_limi2_2 = min(two_j10+two_j7,two_j1+two_j2);
    two_limi3_1 = max(abs(two_j9-two_j8),abs(two_j3-two_j1));
    two_limi3_2 = min(two_j9+two_j8,two_j3+two_j1);
    two_limi4_1 = max(abs(two_j5-two_j6),abs(two_j10-two_j9));
    two_limi4_2 = min(two_j5+two_j6,two_j10+two_j9);
    two_limi5_1 = max(abs(two_j4-two_j6),abs(two_j7-two_j8));
    two_limi5_2 = min(two_j4+two_j6,two_j7+two_j8);

    //////////////////// Loading Hash Tables ////////////////////

    char pathRead2[200];
    char pathRead3[200];
    char pathRead4[200];
    char pathRead5[200];

    sprintf(pathRead2,"../data/HashTablesJ6/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.6j",
            two_j1, two_j2, two_j3, two_j4, two_j5,
            two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);
    sprintf(pathRead3,"../data/HashTablesCoherent/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i.coh",
            two_j1, two_j2, two_j3, two_j4, two_j5,
            two_j6, two_j7, two_j8, two_j9, two_j10);
    sprintf(pathRead4,"../data/Immirzi_%.2f/HashTablesBooster/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.boost",
            ImmirziSave, two_j1, two_j2, two_j3, two_j4, two_j5,
            two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);
    sprintf(pathRead5, "../data/Immirzi_%.2f/HashTablesSL2C/CutOff_%i/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.sl2c",
                    ImmirziSave, cutoff_gaussian, two_j1, two_j2, two_j3, two_j4,two_j5,
                    two_j6, two_j7, two_j8, two_j9, two_j10,two_Dl);

    khash_t(HashTableJ6) *h = kh_load(HashTableJ6, pathRead2);
    khash_t(HashTableCoherent) *h1 = kh_load(HashTableCoherent, pathRead3);
    khash_t(HashTableBooster) *h2 = kh_load(HashTableBooster, pathRead4);

    //////////////////// Check if SL2C data already exists
    //////////////////// initialize or load previously computed values

    khash_t(HashTableMagnetic) *h3 = NULL;

    unsigned int Entries=0;
    char pathRead6[200];

    for ( int i = two_Dl;  i >= 0; i-=2){

      sprintf(pathRead6, "../data/Immirzi_%.2f/HashTablesSL2C/CutOff_%i/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.sl2c",
            ImmirziSave, cutoff_gaussian, two_j1, two_j2, two_j3, two_j4,two_j5, two_j6, two_j7, two_j8, two_j9, two_j10,i);
      if(file_exist (pathRead6) != 0 ){

          h3 = kh_load(HashTableMagnetic, pathRead6);
          Entries = kh_size(h3);
          break;
      }
    }

    if ( h3 == NULL){

        h3 = kh_init(HashTableMagnetic);
    }

    //////////////// Initialize SL2C values and MPFR class ////////////////

    mpfr_rnd_t  rnd = GMP_RNDN;
    double complex Sl2cValue = 0.0 + 0.0*I;
    double complex err = 0.0 + 0.0*I;

    //////////////////// Booster Array from HashTable ////////////////////

    double boost_value;
    HashTableBooster_key_t boost_key ;
    //I store a variable with the lowest B4's
    // value to provide a cutoff over K spins
    double min = 1.;
    int key;
    //Retrieve all values from BoosterHashTable and store
    //them in an array: given a key I can find all i,k
    //intertwiners, useful for cache

    #if (CACHE && SIMMETRIC)
    kh_foreach(h2,boost_key,boost_value,{
      key =  B4_Hash_Symmetric (boost_key.k[0], boost_key.k[1], boost_key.k[2], boost_key.k[3],
                                boost_key.k[4], boost_key.k[5], boost_key.k[6], boost_key.k[7]);
      if (B4[key][boost_key.k[8]][boost_key.k[9]] != 0 && boost_value != 0 ){
        printf ("B4 Hash Error\n");
        exit(0);
      }
      //printf("key = %i\n", key);
      B4[key][boost_key.k[8]][boost_key.k[9]] = boost_value;
      if(fabs(boost_value) < min && fabs(boost_value) > pow(10,-40)){
        min = fabs(boost_value);
      }
    })
    #elif (CACHE && !SIMMETRIC )
    kh_foreach(h2,boost_key,boost_value,{
      key =  B4_Hash_ASymmetric (boost_key.k[0], boost_key.k[1], boost_key.k[2], boost_key.k[3],
                                 boost_key.k[4], boost_key.k[5], boost_key.k[6], boost_key.k[7]);
      if (B4[key][boost_key.k[8]][boost_key.k[9]] != 0 && boost_value != 0 ){
        printf ("B4 Hash Error\n");
        exit(0);
      }
      B4[key][boost_key.k[8]][boost_key.k[9]] = boost_value;
      if(fabs(boost_value) < min && fabs(boost_value) > pow(10,-40)){
        min = fabs(boost_value);
      }
    })
    #else
      kh_foreach(h2,boost_key,boost_value,{
      if(fabs(boost_value) < min && fabs(boost_value) > pow(10,-40)){
        min = fabs(boost_value);
      }
    })
    #endif

    //////////////////// Booster CutOffs ////////////////////

    double CutOff = pow(10,-40);
    double CutOff_K;

    //Two different cutoff, for the lorentzian or for the
    //euclidean configuration. They are empirical.
    if (config_type == 0 && two_Dl != 0 ){
      CutOff_K = pow(min,4)*pow(10,5);
    }
    if (config_type == 1 && two_Dl != 0){
      CutOff_K = pow(min,3)*pow(10,2);
    }


    //////////////////// CoherentStates Array ////////////////////

    #if CACHE
    double complex coherent_value;
    HashTableCoherent_key_t coherent_key ;
    kh_foreach(h1,coherent_key,coherent_value,{
      CoherentStates[coherent_key.k[0]][coherent_key.k[1]] = coherent_value;
    })
    #endif

    //////////////// Gaussian Check for Euclidean and for Lorentzian Data////////////////

    char two_rangei4[2], two_rangei2[2], two_rangei3[2];
    //Provide a cut off over boundary intertwiners
    //using gaussian boundary states
    if (config_type == 0){

      CoherentGaussianCut ( h1, two_rangei2,
                            two_j10, two_j7, two_j1, two_j2,
                            two_N, cutoff_gaussian, 2,
                            config_type);

      CoherentGaussianCut ( h1, two_rangei3,
                            two_j9, two_j8, two_j3, two_j1,
                            two_N, cutoff_gaussian, 3,
                            config_type);

      CoherentGaussianCut ( h1, two_rangei4,
                            two_j6, two_j5, two_j10, two_j9,
                            two_N, cutoff_gaussian, 4,
                            config_type);

    }
    if (config_type == 1){

      two_rangei2[0] = two_limi2_1;
      two_rangei2[1] = two_limi2_2;

      two_rangei3[0] = two_limi3_1;
      two_rangei3[1] = two_limi3_2;

      CoherentGaussianCut ( h1, two_rangei4,
                            two_j6, two_j5, two_j10, two_j9,
                            two_N, cutoff_gaussian, 4,
                            config_type);

    }

    //////////////////// Start L summations ////////////////////

    for (unsigned int two_l1 = two_j1 ; two_l1 <= two_j1+two_Dl; two_l1+=2){
    for (unsigned int two_l2 = two_j2; two_l2 <= two_j2+two_Dl; two_l2+=2){
    for (unsigned int two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
    for (unsigned int two_l4 = two_j4; two_l4 <= two_j4+two_Dl; two_l4+=2){
    for (unsigned int two_l7 = two_j7; two_l7 <= two_j7+two_Dl; two_l7+=2){
    for (unsigned int two_l8 = two_j8; two_l8 <= two_j8+two_Dl; two_l8+=2){

      //////////////////// Initialize SL2C value at fixed L ////////////////////

      double complex Sl2cValueL = 0.0 + 0.0*I;
      double complex errL = 0.0 + 0.0*I;

      //Prepare hash keys for sets of l's
      //Useful for cache

      #if (CACHE && SIMMETRIC)
      int keyB,keyC,keyE,keyA;

      keyB = B4_Hash_Symmetric (two_j10,two_j7,two_j1,two_j2,
                                    two_j10,two_l7,two_l1,two_l2);
      keyC = B4_Hash_Symmetric (two_j9,two_j8,two_j3,two_j1,
                                    two_j9,two_l8,two_l3,two_l1);
      keyE = B4_Hash_Symmetric (two_j4,two_j6,two_j7,two_j8,
                                    two_l4,two_j6,two_l7,two_l8);
      keyA = B4_Hash_Symmetric (two_j2,two_j3,two_j5,two_j4,
                                    two_l2,two_l3,two_j5,two_l4);
      #elif (CACHE && !SIMMETRIC )

      int keyB,keyC,keyE,keyA;

      keyB = B4_Hash_ASymmetric (two_j10,two_j7,two_j1,two_j2,
                                     two_j10,two_l7,two_l1,two_l2);
      keyC = B4_Hash_ASymmetric (two_j9,two_j8,two_j3,two_j1,
                                     two_j9,two_l8,two_l3,two_l1);
      keyE = B4_Hash_ASymmetric (two_j4,two_j6,two_j7,two_j8,
                                     two_l4,two_j6,two_l7,two_l8);
      keyA = B4_Hash_ASymmetric (two_j2,two_j3,two_j5,two_j4,
                                     two_l2,two_l3,two_j5,two_l4);
      #endif

      //////////////////// Range for internal K intertwiners ////////////////////

      unsigned int  two_limk2_1, two_limk2_2,two_limk3_1, two_limk3_2,
                    two_limk5_1, two_limk5_2;

      two_limk2_1 = max(abs(two_j10-two_l7),abs(two_l1-two_l2));
      two_limk2_2 = min(two_j10+two_l7,two_l1+two_l2);
      two_limk3_1 = max(abs(two_j9-two_l8),abs(two_l3-two_l1));
      two_limk3_2 = min(two_j9+two_l8,two_l3+two_l1);
      two_limk5_1 = max(abs(two_l4-two_j6),abs(two_l7-two_l8));
      two_limk5_2 = min(two_l4+two_j6,two_l7+two_l8);

      //////////////////// Prepare Key for SL2C l's value ////////////////////

      HashTableMagnetic_key_t key = {two_l1,two_l2,two_l3,two_l4,two_l7,two_l8};

      //////////////////// Check we haven't already the SL2C l's key/value ////////////////////

      if (kh_get(HashTableMagnetic, h3, key) == kh_end(h3)){

        //////////////////// Start I summations - Nested parallelization ////////////////////

        #pragma omp parallel reduction (+:Sl2cValueL, errL)
        {
          unsigned int two_i1,two_i2,two_i3,two_i4,two_i5;

          #pragma omp for collapse(5)
          for (two_i1 = two_limi1_1; two_i1 <= two_limi1_2; two_i1+=2){
          for (two_i2 = two_rangei2[0]; two_i2 <= two_rangei2[1]; two_i2+=2){
          for (two_i3 = two_rangei3[0]; two_i3 <= two_rangei3[1]; two_i3+=2){
          for (two_i4 = two_rangei4[0]; two_i4 <= two_rangei4[1]; two_i4+=2){
          for (two_i5 = two_limi5_1; two_i5 <= two_limi5_2; two_i5+=2){

            //////////////////// Five Coherent Tetrahedra Multiplication as MPFR variables////////////////////

            double complex states;
            double complex CoherentAux1, CoherentAux2, CoherentAux3;

            #if CACHE
            CoherentAux1 = MPFRComplexMultiplication(CoherentStates[1][two_i1], CoherentStates[2][two_i2]);
            CoherentAux2 = MPFRComplexMultiplication(CoherentAux1, CoherentStates[3][two_i3]);
            CoherentAux3 = MPFRComplexMultiplication(CoherentAux2, CoherentStates[4][two_i4]);
            states =  MPFRComplexMultiplication(CoherentAux3, CoherentStates[5][two_i5]);
            #else
            khint_t cA, cB, cC, cD, cE;

            HashTableCoherent_key_t cohA = {1,two_i1};
            HashTableCoherent_key_t cohB = {2,two_i2};
            HashTableCoherent_key_t cohC = {3,two_i3};
            HashTableCoherent_key_t cohD = {4,two_i4};
            HashTableCoherent_key_t cohE = {5,two_i5};

            cA=kh_get(HashTableCoherent, h1, cohA);
            cB=kh_get(HashTableCoherent, h1, cohB);
            cC=kh_get(HashTableCoherent, h1, cohC);
            cD=kh_get(HashTableCoherent, h1, cohD);
            cE=kh_get(HashTableCoherent, h1, cohE);

            CoherentAux1 = MPFRComplexMultiplication(kh_value(h1, cA), kh_value(h1, cB));
            CoherentAux2 = MPFRComplexMultiplication(CoherentAux1, kh_value(h1, cC));
            CoherentAux3 = MPFRComplexMultiplication(CoherentAux2, kh_value(h1, cD));
            states =  MPFRComplexMultiplication(CoherentAux3, kh_value(h1, cE));

            #endif

            //////////////////// Initialize pointers for 6j keys  ////////////////////

            char *A = malloc(6*sizeof(int));
            char *B = malloc(6*sizeof(int));
            char *C = malloc(6*sizeof(int));
            char *D = malloc(6*sizeof(int));
            char *E = malloc(6*sizeof(int));

            //////////////////// Start K summations ////////////////////

            for(unsigned int  two_k2 = two_limk2_1; two_k2 <= two_limk2_2; two_k2+=2){

              double boosterB, boosterC, boosterE, boosterA;

              #if CACHE
                //////////////////// Get booster functions from arrays ////////////////////
                boosterB = B4[keyB][two_i2][two_k2];
                if ( fabs(boosterB) < CutOff ) continue;
              #else
                //////////////////// Get booster functions from Hash Table ////////////////////
                khint_t bB;
                HashTableBooster_key_t boostB = {two_j10, two_j7, two_j1, two_j2,
                                                 two_j10,two_l7,two_l1,two_l2,
                                                 two_i2,two_k2};
                bB=kh_get(HashTableBooster, h2, boostB);
                boosterB = kh_value(h2, bB);
                if ( fabs(boosterB) < CutOff ) continue;

              #endif

            for(unsigned int  two_k3 = two_limk3_1; two_k3 <= two_limk3_2; two_k3+=2){

              #if CACHE
                boosterC = B4[keyC][two_i3][two_k3];
                if ( fabs(boosterC) < CutOff ) continue;
              #else
                khint_t bC;
                HashTableBooster_key_t boostC = {two_j9,two_j8,two_j3,two_j1,
                                                 two_j9,two_l8,two_l3,two_l1,
                                                 two_i3,two_k3};
                bC=kh_get(HashTableBooster, h2, boostC);
                boosterC = kh_value(h2, bC);
                if ( fabs(boosterC) < CutOff ) continue;
              #endif

            for(unsigned int  two_k5 = two_limk5_1; two_k5 <= two_limk5_2; two_k5+=2){

              #if CACHE
                boosterE = B4[keyE][two_i5][two_k5];
                if ( fabs(boosterE) < CutOff ) continue;
              #else
                khint_t bE;
                HashTableBooster_key_t boostE = {two_j4,two_j6,two_j7,two_j8,
                                                 two_l4,two_j6,two_l7,two_l8,
                                                 two_i5,two_k5};
                bE=kh_get(HashTableBooster, h2, boostE);
                boosterE = kh_value(h2, bE);
                if ( fabs(boosterE) < CutOff ) continue;
              #endif

            for(unsigned int  two_k1 = max(max(abs(two_l3-two_l2),abs(two_j5-two_l4)),max(abs(two_k3-two_k2),abs(two_k5-two_i4)));
                              two_k1 <= min(min(two_l3+two_l2,two_j5+two_l4),min(two_k2+two_k3,two_k5+two_i4)); two_k1+=2){

            #if CACHE
              boosterA = B4[keyA][two_i1][two_k1];
              if ( fabs(boosterA) < CutOff ) continue;
            #else
              khint_t bA;
              HashTableBooster_key_t boostA = {two_j2,two_j3,two_j5,two_j4,
                                               two_l2,two_l3,two_j5,two_l4,
                                               two_i1,two_k1};
              bA=kh_get(HashTableBooster, h2, boostA);
              boosterA = kh_value(h2, bA);
              if ( fabs(boosterA) < CutOff ) continue;
            #endif

            //printf("%17g \n", fabs(boosterA*boosterB*boosterC*boosterE));

            #if BOOSTERS_K_CUTOFF
              if( fabs(boosterA*boosterB*boosterC*boosterE) < fabs(CutOff_K)) continue;
            #endif

            /////////////////////////////////////
            //Convert Boosters to MPFR variables
            /////////////////////////////////////

            mpfr_t  boosterMPFR_A, boosterMPFR_B, boosterMPFR_C, boosterMPFR_E, boostersMPFR ;

            mpfr_init_set_d (boosterMPFR_A, boosterA, GMP_RNDN);
            mpfr_init_set_d (boosterMPFR_B, boosterB, GMP_RNDN);
            mpfr_init_set_d (boosterMPFR_C, boosterC, GMP_RNDN);
            mpfr_init_set_d (boosterMPFR_E, boosterE, GMP_RNDN);
            mpfr_init (boostersMPFR);

            ///////////////////////////////////
            //Multiply Boosters as MPFR variables
            ///////////////////////////////////

            mpfr_mul (boostersMPFR, boosterMPFR_A, boosterMPFR_B, GMP_RNDN);
            mpfr_mul (boostersMPFR, boostersMPFR, boosterMPFR_C, GMP_RNDN);
            mpfr_mul (boostersMPFR, boostersMPFR, boosterMPFR_E, GMP_RNDN);

            mpfr_clears ( boosterMPFR_A, boosterMPFR_B,
                          boosterMPFR_C, boosterMPFR_E,
                          NULL);

            //////////////////// Compute 15j Symbol  ////////////////////

            double val15j = J15Symbol ( h,
                                        &A, &B, &C, &D,  &E,
                                        two_l1,   two_l2,  two_l3, two_l4, two_j5,
                                        two_j6,   two_l7,  two_l8, two_j9, two_j10,
                                        two_i1,   two_i2,  two_i3, two_i4, two_i5,
                                        two_k1,   two_k2,  two_k3, two_k5
                                      );

           ///////////////////////////////////////////////////////////
           //Convert 15j symbol and coherent states to MPFR variables
           ///////////////////////////////////////////////////////////

            mpfr_t  val15jMPFR;
            mpfr_init_set_d (val15jMPFR, val15j, GMP_RNDN);

            double complex statesNormalized = states * d(two_i1)*d(two_i2)*d(two_i3)*d(two_i4)*d(two_i5)*
                                                       d(two_k1)*d(two_k2)*d(two_k3)*d(two_k5);
            mpfr_t  statesMPFR_Re, statesMPFR_Im;

            mpfr_init_set_d (statesMPFR_Re, creal(statesNormalized), GMP_RNDN);
            mpfr_init_set_d (statesMPFR_Im, cimag(statesNormalized), GMP_RNDN);

            ////////////////////////////////////////
            //Compute SL2C value as an MPFR variable
            ////////////////////////////////////////

            mpfr_t  Sl2cValueMPFR_Re, Sl2cValueMPFR_Im, AuxMPFR;

            mpfr_init (Sl2cValueMPFR_Re);
            mpfr_init (Sl2cValueMPFR_Im);
            mpfr_init (AuxMPFR);

            mpfr_mul (AuxMPFR, val15jMPFR, boostersMPFR, GMP_RNDN);

            mpfr_mul (Sl2cValueMPFR_Re, statesMPFR_Re, AuxMPFR, GMP_RNDN);
            mpfr_mul (Sl2cValueMPFR_Im, statesMPFR_Im, AuxMPFR, GMP_RNDN);

            /////////////////////////////////////////////////////////
            //Back to double and Kahan compensated summation for I,K
            ////////////////////////////////////////////////////////

            CompensatedSummationMPFR (&errL, &Sl2cValueL, Sl2cValueMPFR_Re, Sl2cValueMPFR_Im );

            ////////////////////////////
            //Free MPFR variables memory
            ////////////////////////////

            mpfr_clears ( boostersMPFR, AuxMPFR, val15jMPFR,
                          statesMPFR_Re, statesMPFR_Im,
                          Sl2cValueMPFR_Re, Sl2cValueMPFR_Im,
                          NULL);
            }
            }
            }
            }

          ///////////////////
          //Free 6j pointers
          //////////////////

          free(A);
          free(B);
          free(C);
          free(D);
          free(E);

          }
          }
          }
          }
          }
        }

        //////////////////// Put key and value in SL2C l's HashTable ////////////////////

        int ret;
        khint_t s;

        s = kh_put(HashTableMagnetic, h3, key, &ret);

        if ( ret == 1 ){
          Entries = Entries+1;
          kh_val(h3,s) = Sl2cValueL;
          s=kh_get(HashTableMagnetic, h3, key);
          if( kh_val(h3,s) == 0 || kh_val(h3,s) != Sl2cValueL){
            kh_val(h3,s) = Sl2cValueL;
          }
        }

        ////////////////////////////
        //// Summation for L
        ////////////////////////////

        Sl2cValue +=  Sl2cValueL;

      }
      //////////////////// If we already have SL2C l's value, get it and sum ////////////////////
      else{
        khint_t s;
        s = kh_get(HashTableMagnetic, h3, key);
        Sl2cValueL = kh_val(h3,s);
        Sl2cValue +=  Sl2cValueL;
      }


    }
    }
    }
    }
    }
    }


    //////////////////// Check L hash table entries and save it////////////////////

    if (kh_size(h3) != pow(1+two_Dl/2,6)){
      printf("Hashing Problem for two_Dl = %i", two_Dl);
    }

    #if SAVE_DATA
    kh_write(HashTableMagnetic, h3, pathRead5);
    #endif

    //////////////////// Get 4-simplex Dihedral angle ////////////////////

    char configDihedral[200];
    sprintf(configDihedral, "%s.SumDihedralAngles",argv[1]);
    SumDihedralAngles = config_lookup(cf, configDihedral);
    double DihedralAngle = config_setting_get_float(SumDihedralAngles);

    //////////////////// Saddle Point Phase and print result////////////////////

    double complex saddle = cexp(-I*DihedralAngle*two_N/2);
    double complex Sl2cValueSaddle = saddle*Sl2cValue;

    printf("\nSL2C ValueSaddle: %i %i %i %i %i %i %i %i %i %i | %i | %17g %17g*I \n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5,
          two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl,
          creal(Sl2cValueSaddle),cimag(Sl2cValueSaddle));

    //////////////////// Save Data ////////////////////

    #if SAVE_DATA
    FILE *f = fopen(pathRead1, "w");
    if (f != NULL){

      fprintf(f,"%17g%17g%17g%17g\n", creal(Sl2cValue), cimag (Sl2cValue),
                                      creal(Sl2cValueSaddle),cimag(Sl2cValueSaddle));
    }
    fclose(f);


    #endif

    //////////////////// Free Memory from HashTables ////////////////////

    kh_destroy(HashTableJ6, h);
    kh_destroy(HashTableCoherent, h1);
    kh_destroy(HashTableMagnetic, h3);
    kh_destroy(HashTableBooster, h2);

  }
  else{

    printf("\nWe have already computed |%i %i %i %i %i %i %i %i %i %i| %i | SL2C Symbol\n\n",
            two_j1, two_j2, two_j3, two_j4, two_j5,
            two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);
  }

  //////////////////// Algorithm for dynamical shell precision  ////////////////////

  if ( two_Dl != 0 ){

    char pathReadCheck[200];
    sprintf(pathReadCheck,"../data/Immirzi_%.2f/SL2CResults/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i._%i.txt",
            ImmirziSave, two_j1, two_j2, two_j3, two_j4, two_j5,
            two_j6, two_j7, two_j8, two_j9, two_j10,two_Dl-2);

    FILE *f1 = fopen(pathRead1, "r");
    if (f1 != NULL){

      float value1, value2;
      fscanf(f1,"%f %f ",&value1,&value2);

      FILE *fCheck = fopen(pathReadCheck, "r");
      if (fCheck != NULL){

        float value3, value4;
        fscanf(fCheck,"%f %f ",&value3,&value4);
        double err;

        //////////////////// Relative error
        //////////////////// between shell two_Dl and two_Dl -2

        err = 100*fabs( sqrt(pow(value1,2)+pow(value2,2))-sqrt(pow(value3,2)
                        +pow(value4,2)))/sqrt(pow(value1,2)+pow(value2,2));

        //////////////////// Get Admitted error ////////////////////

        char configErr[200];
        sprintf(configErr, "%s.ErrorPer",argv[1]);
        ErrorPer = config_lookup(cf, configErr);
        double Error = config_setting_get_float(ErrorPer);

        //////////////////// Check relative error < Admitted error ////////////////////

        if (err <= Error){

          config_destroy(cf);
          //////////////////// Return 1 is used by bash file
          //////////////////// to exit the process
          return 1;
        }
      }
      fclose (fCheck);
    }
    fclose (f1);
  }
  config_destroy(cf);
  return 0;
}
