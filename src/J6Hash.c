// gcc-7 -std=c99 -fopenmp -lgmp -lmpfr -lmpc -Lwigxjpf-1.5/lib -Iwigxjpf-1.5/inc -Iinc src/J6Hash.c -lm -lwigxjpf -lconfig -o bin/J6Hash

#include "Common.h"
#include "JSymbols.h"   //Canonicalization of 6j symbols
#include "Utilities.h"            //Various Functions
#include "khash.h"                //Hash Table
#include "wigxjpf.h"              //To compute 3j,6j symbols

//////////////////// Initialize Hash Table ////////////////////

ARRAY_TABLE_INIT(HashTable, 6*sizeof(int), double); // 24 bytes keys (6 int * 4 bytes) with double values.

int main(int argc, char **argv){

  //////////////////// Initialize configuration file ////////////////////

  config_t cfg, *cf;
  const config_setting_t *Two_Spins;

  unsigned int two_j1Min, two_j2Min, two_j3Min, two_j4Min, two_j5Min, two_j6Min, two_j7Min, two_j8Min, two_j9Min, two_j10Min;

  cf = &cfg;
  config_init(cf);

  //////////////////// Check configuration file ////////////////////

  if (!config_read_file(cf, "../config/SL2Configurations.txt")){

      fprintf(stderr, "%s:%d - %s\n",
              config_error_file(cf),
              config_error_line(cf),
              config_error_text(cf));
      config_destroy(cf);
      return(EXIT_FAILURE);
  }

  //////////////////// Initialize minimum boundary spins ////////////////////

  char configTwo_Spins[200];
  sprintf(configTwo_Spins, "%s.Two_Spins",argv[1]);

  Two_Spins = config_lookup(cf, configTwo_Spins);
  two_j1Min = config_setting_get_int_elem(Two_Spins, 0);
  two_j2Min = config_setting_get_int_elem(Two_Spins, 1);
  two_j3Min = config_setting_get_int_elem(Two_Spins, 2);
  two_j4Min = config_setting_get_int_elem(Two_Spins, 3);
  two_j5Min = config_setting_get_int_elem(Two_Spins, 4);
  two_j6Min = config_setting_get_int_elem(Two_Spins, 5);
  two_j7Min = config_setting_get_int_elem(Two_Spins, 6);
  two_j8Min = config_setting_get_int_elem(Two_Spins, 7);
  two_j9Min = config_setting_get_int_elem(Two_Spins, 8);
  two_j10Min = config_setting_get_int_elem(Two_Spins, 9);

  //////////////////// Rescaling parameters and Dl ////////////////////

  unsigned int two_N = atoi(argv[2]);
  unsigned int two_Dl = atoi(argv[3]);

  unsigned int two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10;

  two_j1 = two_N*two_j1Min; two_j2 = two_N*two_j2Min; two_j3 = two_N*two_j3Min; two_j4 = two_N*two_j4Min; two_j5 = two_N*two_j5Min;
  two_j6 = two_N*two_j6Min; two_j7 = two_N*two_j7Min; two_j8 = two_N*two_j8Min; two_j9 = two_N*two_j9Min; two_j10 = two_N*two_j10Min;

  char folderPath[200] = "../data/HashTablesJ6";
  struct stat st = {0};

  if (stat(folderPath, &st) == -1) {

    char folderPath1[200] = "../data";

    if (stat(folderPath1, &st) == -1) {
    mkdir(folderPath1, 0700);
    mkdir(folderPath, 0700);
    }
    else{
      mkdir(folderPath, 0700);
    }

  }


  char pathRead[200];
  sprintf(pathRead, "../data/HashTablesJ6/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.6j",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);

  //////////////////// Check if data already exists and load previopusly computed tables ////////////////////

  if (file_exist (pathRead) == 0 ){

    wig_table_init(2*1000, 6);
    wig_temp_init(2*1000);

    khash_t(HashTable) *h = NULL;

    char pathRead1[200];

    unsigned int Entries=0;

    for ( int i = two_Dl;  i >= 0; i-=2){

        sprintf(pathRead1, "../data/HashTablesJ6/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.6j",
                two_j1, two_j2, two_j3, two_j4,two_j5, two_j6, two_j7, two_j8, two_j9, two_j10,i);
        if(file_exist (pathRead1) != 0 ){

            h = kh_load(HashTable, pathRead1);
            Entries = kh_size(h);
            break;
        }
    }

    if ( h == NULL){

        h = kh_init(HashTable);
    }

    //////////////////// Range for boundary intertwiner ////////////////////

    unsigned int two_limi4_1, two_limi4_2;

    two_limi4_1=max(abs(two_j5-two_j6),abs(two_j10-two_j9));
    two_limi4_2=min(two_j5+two_j6,two_j10+two_j9);

    unsigned int Combinations = 0;

    double val6jA,val6jB,val6jC,val6jD,val6jE;

    char *A = malloc(6*sizeof(int));
    char *B = malloc(6*sizeof(int));
    char *C = malloc(6*sizeof(int));
    char *D = malloc(6*sizeof(int));
    char *E = malloc(6*sizeof(int));

    for (unsigned int two_l1 = two_j1 ; two_l1 <= two_j1+two_Dl; two_l1+=2){
    for (unsigned int two_l2 = two_j2; two_l2 <= two_j2+two_Dl; two_l2+=2){
    for (unsigned int two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
    for (unsigned int two_l4 = two_j4; two_l4 <= two_j4+two_Dl; two_l4+=2) {
    for (unsigned int two_l7 = two_j7; two_l7 <= two_j7+two_Dl; two_l7+=2){
    for (unsigned int two_l8 = two_j8; two_l8 <= two_j8+two_Dl; two_l8+=2){
    for(unsigned int two_i4 = two_limi4_1; two_i4 <= two_limi4_2; two_i4+=2){
    for(unsigned int two_k2 = max(abs(two_j10-two_l7),abs(two_l1-two_l2)); two_k2 <= min(two_j10+two_l7,two_l1+two_l2); two_k2+=2){
    for(unsigned int two_k3 = max(abs(two_j9-two_l8),abs(two_l3-two_l1)); two_k3 <= min(two_j9+two_l8,two_l3+two_l1); two_k3+=2){
    for(unsigned int two_k5 = max(abs(two_l4-two_j6),abs(two_l7-two_l8)); two_k5 <= min(two_l4+two_j6,two_l7+two_l8); two_k5+=2){
    for(unsigned int two_k1 = max(max(abs(two_l3-two_l2),abs(two_j5-two_l4)),max(abs(two_k3-two_k2),abs(two_k5-two_i4)));
                     two_k1 <= min(min(two_l3+two_l2,two_j5+two_l4),min(two_k2+two_k3,two_k5+two_i4)); two_k1+=2){
      khint_t kA,kB,kC,kD,kE;
      int retA,retB;

      Combinations = Combinations + 1;

      ///////////////////////////////////////
      //6j Symbol A - Key conversion and save
      ///////////////////////////////////////

      WIGNER6J_REGGE_CANONICALISE(&A, two_k1, two_k3, two_k2, two_l1, two_l2, two_l3);

      HashTable_key_t keyA = {A[0],A[1],A[2],A[3],A[4],A[5]};
      kA=kh_put(HashTable, h, keyA, &retA);

      if ( retA == 1 ){
        Entries = Entries+1;
        val6jA = wig6jj(two_k1,two_k3,two_k2,two_l1,two_l2,two_l3);
        kh_val(h,kA) = val6jA;
        kA=kh_get(HashTable, h, keyA);
        if( kh_val(h,kA) == 0 || kh_val(h,kA) != val6jA){
            kh_val(h,kA) = val6jA;
        }
      }

      ///////////////////////////////////////
      //6j Symbol B - Key conversion and save
      ///////////////////////////////////////

      WIGNER6J_REGGE_CANONICALISE(&B, two_k1, two_i4, two_k5, two_j6, two_l4, two_j5);

      HashTable_key_t keyB = {B[0],B[1],B[2],B[3],B[4],B[5]};
      kB=kh_put(HashTable, h, keyB, &retB);

      if ( retB == 1 ){
        Entries = Entries+1;
        val6jB = wig6jj(two_k1, two_i4, two_k5, two_j6, two_l4, two_j5);
        kh_val(h,kB) = val6jB;
        kB=kh_get(HashTable, h, keyB);
        if( kh_val(h,kB) == 0 || kh_val(h,kB) != val6jB){
            kh_val(h,kB) = val6jB;
        }
      }

      unsigned int two_limx1 = max(max(abs(two_k2-two_k5),abs(two_j10-two_l8)),abs(two_k3-two_i4));
      unsigned int two_limx2 = min(min(two_k2+two_k5,two_j10+two_l8),two_k3+two_i4);

      ////////////////////////////
      //9j via 6j summation
      ////////////////////////////

      for( unsigned int two_x = two_limx1; two_x <= two_limx2; two_x+=2){

        //////////////////////////////////////////////
        //6j Symbol C for 9j - Key conversion and save
        //////////////////////////////////////////////

        int retC,retD,retE;

        WIGNER6J_REGGE_CANONICALISE(&C, two_k2,two_j10,two_l7,two_l8,two_k5,two_x);

        HashTable_key_t keyC = {C[0],C[1],C[2],C[3],C[4],C[5]};
        kC=kh_put(HashTable, h, keyC, &retC);

        if ( retC== 1 ){
          Entries = Entries+1;
          val6jC = wig6jj(two_k2,two_j10,two_l7,two_l8,two_k5,two_x);
          kh_val(h,kC) = val6jC;
          kC=kh_get(HashTable, h, keyC);
          if( kh_val(h,kB) == 0 || kh_val(h,kC) != val6jC){
              kh_val(h,kC) = val6jC;
          }
        }

        //////////////////////////////////////////////
        //6j Symbol D for 9j - Key conversion and save
        //////////////////////////////////////////////

        WIGNER6J_REGGE_CANONICALISE(&D, two_k3,two_j9,two_l8,two_j10,two_x,two_i4);

        HashTable_key_t keyD= {D[0],D[1],D[2],D[3],D[4],D[5]};
        kD=kh_put(HashTable, h, keyD, &retD);

        if ( retD == 1 ){
          Entries = Entries+1;
          val6jD = wig6jj(two_k3,two_j9,two_l8,two_j10,two_x,two_i4);
          kh_val(h,kD) = val6jD;
          kD=kh_get(HashTable, h, keyD);
          if( kh_val(h,kD) == 0 || kh_val(h,kD) != val6jD){
              kh_val(h,kD) = val6jD;
          }
        }

        //////////////////////////////////////////////
        //6j Symbol E for 9j - Key conversion and save
        //////////////////////////////////////////////-

        WIGNER6J_REGGE_CANONICALISE(&E, two_k1,two_i4,two_k5,two_x,two_k2,two_k3);

        HashTable_key_t keyE = {E[0],E[1],E[2],E[3],E[4],E[5]};
        kE=kh_put(HashTable, h, keyE, &retE);

        if ( retE == 1 ){
          Entries = Entries+1;
          val6jE = wig6jj(two_k1,two_i4,two_k5,two_x,two_k2,two_k3);
          kh_val(h,kE) = val6jE;
          kE=kh_get(HashTable, h, keyE);
          if( kh_val(h,kE) == 0 || kh_val(h,kE) != val6jE){
            kh_val(h,kE) = val6jE;
          }
        }
      }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }

    free(A);
    free(B);
    free(C);
    free(D);
    free(E);

    //////////////////// Write the hash table to disk ////////////////////

    kh_write(HashTable, h, pathRead);

    printf("\nCombinations for 6j Symbols: %i\n", Combinations);
    printf("Hash Entries for 6j Symbols: %d \n\n", kh_size(h));

    //////////////////// Consistency check between entries and hash table size ////////////////////

    if ( Entries != kh_size(h) ){

      printf("Alert: Entries != Table size for 6j Symbols \n");
      return 0;
    }

    //////////////////// Free Memory ////////////////////

    void wig_temp_free();
    void wig_table_free();

    kh_destroy(HashTable, h);
  }
  else{
    printf("\nWe have already computed |%i %i %i %i %i %i %i %i %i %i| %i |  6J Hash Table\n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);
  }
  config_destroy(cf);
  return 0;
}
