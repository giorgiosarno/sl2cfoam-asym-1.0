// gcc-7 -std=c99 -fopenmp -lgmp -lmpfr -lmpc -Lwigxjpf-1.5/lib -Iwigxjpf-1.5/inc -Iinc src/CoherentStates.c -lm -lwigxjpf -lconfig -o bin/CoherentStates

#include "Common.h"
#include "CoherentStatesFunctions.h" //WignerDFunctions
#include "Utilities.h"      //various utilities
#include "khash.h"          //Hash Table
#include "wigxjpf.h"        //To compute 3j,6j symbols

//TODO  Maybe would be better to parallelize also CoherentStates
//      Create an array with all values and then bump them in memory
//      As in B4Hash/B4 Function. 

//////////////////// Initialize Hash Table ////////////////////

ARRAY_TABLE_INIT(HashTable, 2*sizeof(int), double complex);  // 8 bytes keys (2 int * 4 bytes) with double complex values.

int main(int argc, char **argv){

  //////////////////// Initialize configuration file ////////////////////

  config_t cfg, *cf;
  const config_setting_t *Two_Spins;
  const config_setting_t *AnglesA;
  const config_setting_t *AnglesB;
  const config_setting_t *AnglesC;
  const config_setting_t *AnglesD;
  const config_setting_t *AnglesE;

  unsigned int two_j1Min, two_j2Min, two_j3Min, two_j4Min, two_j5Min, two_j6Min, two_j7Min, two_j8Min, two_j9Min, two_j10Min;

  double fA1,fA2,fA3,fA4,tA1,tA2,tA3,tA4;
  double fB1,fB2,fB3,fB4,tB1,tB2,tB3,tB4;
  double fC1,fC2,fC3,fC4,tC1,tC2,tC3,tC4;
  double fD1,fD2,fD3,fD4,tD1,tD2,tD3,tD4;
  double fE1,fE2,fE3,fE4,tE1,tE2,tE3,tE4;

  cf = &cfg;
  config_init(cf);

  //////////////////// Check configuration file ////////////////////

  if (!config_read_file(cf, "../config/SL2Configurations.txt"))
  {
      fprintf(stderr, "%s:%d - %s\n",
              config_error_file(cf),
              config_error_line(cf),
              config_error_text(cf));
      config_destroy(cf);
      return(EXIT_FAILURE);
  }

  //////////////////// Initialize minimum boundary spins ////////////////////

  char configTwo_Spins[200];
  sprintf(configTwo_Spins, "%s.Two_Spins",argv[1]);

  Two_Spins = config_lookup(cf, configTwo_Spins);
  two_j1Min = config_setting_get_int_elem(Two_Spins, 0);
  two_j2Min = config_setting_get_int_elem(Two_Spins, 1);
  two_j3Min = config_setting_get_int_elem(Two_Spins, 2);
  two_j4Min = config_setting_get_int_elem(Two_Spins, 3);
  two_j5Min = config_setting_get_int_elem(Two_Spins, 4);
  two_j6Min = config_setting_get_int_elem(Two_Spins, 5);
  two_j7Min = config_setting_get_int_elem(Two_Spins, 6);
  two_j8Min = config_setting_get_int_elem(Two_Spins, 7);
  two_j9Min = config_setting_get_int_elem(Two_Spins, 8);
  two_j10Min = config_setting_get_int_elem(Two_Spins, 9);

   //////////////////// Rescaling parameters ////////////////////

  unsigned int two_N = atoi(argv[2]);

  unsigned int two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10;

  two_j1 = two_N*two_j1Min; two_j2 = two_N*two_j2Min; two_j3 = two_N*two_j3Min; two_j4 = two_N*two_j4Min; two_j5 = two_N*two_j5Min;
  two_j6 = two_N*two_j6Min; two_j7 = two_N*two_j7Min; two_j8 = two_N*two_j8Min; two_j9 = two_N*two_j9Min; two_j10 = two_N*two_j10Min;

  char folderPath[200] = "../data/HashTablesCoherent";
  struct stat st = {0};

  if (stat(folderPath, &st) == -1) {

    char folderPath1[200] = "../data";

    if (stat(folderPath1, &st) == -1) {
    mkdir(folderPath1, 0700);
    mkdir(folderPath, 0700);
    }
    else{
      mkdir(folderPath, 0700);
    }

  }

  char pathRead[200];

  sprintf(pathRead,"../data/HashTablesCoherent/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i.coh",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10);

  //////////////////// Check if data already exists ////////////////////

  if (file_exist (pathRead) == 0 ){

    khash_t(HashTable) *h = kh_init(HashTable);

    wig_table_init(2*1000, 3);
    wig_temp_init(2*1000);

    //////////////////// Range for boundary intertwiner ////////////////////

    unsigned int limiA1= max(abs(two_j2-two_j3),abs(two_j5-two_j4));
    unsigned int limiA2 = min(two_j2+two_j3, two_j5+ two_j4);
    unsigned int limiB1= max(abs(two_j10-two_j7),abs(two_j1-two_j2));
    unsigned int limiB2 = min(two_j10+two_j7, two_j1+ two_j2);
    unsigned int limiC1= max(abs(two_j9-two_j8),abs(two_j3-two_j1));
    unsigned int limiC2 = min(two_j9+two_j8, two_j3+ two_j1);
    unsigned int limiD1= max(abs(two_j6-two_j5),abs(two_j10-two_j9));
    unsigned int limiD2 = min(two_j6+two_j5, two_j10+ two_j9);
    unsigned int limiE1= max(abs(two_j4-two_j6),abs(two_j7-two_j8));
    unsigned int limiE2 = min(two_j4+two_j6, two_j7+ two_j8);

    //////////////////// Initialize configuration angles ////////////////////
    /////////////// Phi and Theta for five boundary tetrahedra //////////////

    char configAnglesA[200];
    sprintf(configAnglesA, "%s.AnglesA",argv[1]);

    AnglesA = config_lookup(cf, configAnglesA);
    fA1 = config_setting_get_float_elem(AnglesA, 0);
    tA1 = config_setting_get_float_elem(AnglesA, 1);
    fA2 = config_setting_get_float_elem(AnglesA, 2);
    tA2 = config_setting_get_float_elem(AnglesA, 3);
    fA3 = config_setting_get_float_elem(AnglesA, 4);
    tA3 = config_setting_get_float_elem(AnglesA, 5);
    fA4 = config_setting_get_float_elem(AnglesA, 6);
    tA4 = config_setting_get_float_elem(AnglesA, 7);

    char configAnglesB[200];
    sprintf(configAnglesB, "%s.AnglesB",argv[1]);

    AnglesB = config_lookup(cf, configAnglesB);
    fB1 = config_setting_get_float_elem(AnglesB, 0);
    tB1 = config_setting_get_float_elem(AnglesB, 1);
    fB2 = config_setting_get_float_elem(AnglesB, 2);
    tB2 = config_setting_get_float_elem(AnglesB, 3);
    fB3 = config_setting_get_float_elem(AnglesB, 4);
    tB3 = config_setting_get_float_elem(AnglesB, 5);
    fB4 = config_setting_get_float_elem(AnglesB, 6);
    tB4 = config_setting_get_float_elem(AnglesB, 7);

    char configAnglesC[200];
    sprintf(configAnglesC, "%s.AnglesC",argv[1]);

    AnglesC = config_lookup(cf, configAnglesC);
    fC1 = config_setting_get_float_elem(AnglesC, 0);
    tC1 = config_setting_get_float_elem(AnglesC, 1);
    fC2 = config_setting_get_float_elem(AnglesC, 2);
    tC2 = config_setting_get_float_elem(AnglesC, 3);
    fC3 = config_setting_get_float_elem(AnglesC, 4);
    tC3 = config_setting_get_float_elem(AnglesC, 5);
    fC4 = config_setting_get_float_elem(AnglesC, 6);
    tC4 = config_setting_get_float_elem(AnglesC, 7);

    char configAnglesD[200];
    sprintf(configAnglesD, "%s.AnglesD",argv[1]);

    AnglesD = config_lookup(cf, configAnglesD);
    fD1 = config_setting_get_float_elem(AnglesD, 0);
    tD1 = config_setting_get_float_elem(AnglesD, 1);
    fD2 = config_setting_get_float_elem(AnglesD, 2);
    tD2 = config_setting_get_float_elem(AnglesD, 3);
    fD3 = config_setting_get_float_elem(AnglesD, 4);
    tD3 = config_setting_get_float_elem(AnglesD, 5);
    fD4 = config_setting_get_float_elem(AnglesD, 6);
    tD4 = config_setting_get_float_elem(AnglesD, 7);

    char configAnglesE[200];
    sprintf(configAnglesE, "%s.AnglesE",argv[1]);

    AnglesE = config_lookup(cf, configAnglesE);
    fE1 = config_setting_get_float_elem(AnglesE, 0);
    tE1 = config_setting_get_float_elem(AnglesE, 1);
    fE2 = config_setting_get_float_elem(AnglesE, 2);
    tE2 = config_setting_get_float_elem(AnglesE, 3);
    fE3 = config_setting_get_float_elem(AnglesE, 4);
    tE3 = config_setting_get_float_elem(AnglesE, 5);
    fE4 = config_setting_get_float_elem(AnglesE, 6);
    tE4 = config_setting_get_float_elem(AnglesE, 7);

    double complex coherentStateiA;
    double complex coherentStateiB;
    double complex coherentStateiC;
    double complex coherentStateiD;
    double complex coherentStateiE;

    int retA,retB,retC,retD,retE ;

    int check = 0;

    /////////////////////////////////////////
    //Coherent State A - Creation and hashing
    /////////////////////////////////////////

    for (unsigned int two_iA = limiA1; two_iA <= limiA2; two_iA+=2){

      khint_t kA;
      coherentStateiA = 0. + 0.*I;
      HashTable_key_t keyA = {1,two_iA};
      kA=kh_put(HashTable, h, keyA, &retA);

      if ( retA == 1 ){
        check = check +1;
        coherentStateiA = CoherentStateI(two_iA, two_j2, two_j3, two_j5, two_j4,
                                         m_pi+(fA1), m_pi-(tA1),
                                         m_pi+(fA2), m_pi-(tA2),
                                         m_pi+(fA3), m_pi-(tA3),
                                         m_pi+(fA4), m_pi-(tA4),
                                         -1,-1,-1,-1);
        kh_value(h, kA) = coherentStateiA;
        kA=kh_get(HashTable, h, keyA);
        if( kh_val(h,kA) == 0 || kh_val(h,kA) != coherentStateiA){
          kh_val(h,kA) = coherentStateiA;
        }
      }
    }

    /////////////////////////////////////////
    //Coherent State B - Creation and hashing
    /////////////////////////////////////////

    for (unsigned int two_iB = limiB1; two_iB <= limiB2; two_iB+=2){

      khint_t kB;
      coherentStateiB = 0. + 0.*I;
      HashTable_key_t keyB = {2,two_iB};
      kB=kh_put(HashTable, h, keyB, &retB);

      if ( retB == 1 ){
        check = check +1;
        coherentStateiB = CoherentStateI(two_iB, two_j10, two_j7, two_j1, two_j2,
                                         m_pi+(fB1), m_pi-(tB1),
                                         m_pi+(fB2), m_pi-(tB2),
                                         m_pi+(fB3), m_pi-(tB3),
                                         +(fB4), +(tB4),
                                         -1,-1,-1, +1);
        kh_value(h, kB) = coherentStateiB;
        kB=kh_get(HashTable, h, keyB);
        if( kh_val(h,kB) == 0 || kh_val(h,kB) != coherentStateiB){
          kh_val(h,kB) = coherentStateiB;
        }
      }
    }

    /////////////////////////////////////////
    //Coherent State C - Creation and hashing
    /////////////////////////////////////////

    for (unsigned int two_iC = limiC1; two_iC <= limiC2; two_iC+=2){

      khint_t kC;
      coherentStateiC = 0. + 0.*I;
      HashTable_key_t keyC = {3,two_iC};
      kC=kh_put(HashTable, h, keyC, &retC);

      if ( retC == 1 ){
        check = check +1;
        coherentStateiC = CoherentStateI(two_iC, two_j9, two_j8, two_j3, two_j1,
                                         m_pi+(fC1), m_pi-(tC1),
                                         m_pi+(fC2), m_pi-(tC2),
                                         +(fC3), +(tC3),
                                         +(fC4), +(tC4),
                                         -1,-1,+1,+1);
        kh_value(h, kC) = coherentStateiC;
        kC=kh_get(HashTable, h, keyC);
        if( kh_val(h,kC) == 0 || kh_val(h,kC) != coherentStateiC){
            kh_val(h,kC) = coherentStateiC;
        }
      }
    }

    /////////////////////////////////////////
    //Coherent State D - Creation and hashing
    /////////////////////////////////////////

    for (unsigned int two_iD = limiD1; two_iD <= limiD2; two_iD+=2){

      khint_t kD;
      coherentStateiD = 0. + 0.*I;
      HashTable_key_t keyD = {4,two_iD};
      kD=kh_put(HashTable, h, keyD, &retD);

      if ( retD == 1 ){
        check = check +1;
        coherentStateiD = CoherentStateI(two_iD, two_j6, two_j5, two_j10, two_j9,
                                         m_pi+(fD1), m_pi-(tD1),
                                         +(fD2), +(tD2),
                                         +(fD3), +(tD3),
                                         +(fD4), +(tD4),
                                         -1,+1,+1,+1);
        kh_value(h, kD) = coherentStateiD;
        kD=kh_get(HashTable, h, keyD);
        if( kh_val(h,kD) == 0 || kh_val(h,kD) != coherentStateiD){
          kh_val(h,kD) = coherentStateiD;
        }
      }
    }

    /////////////////////////////////////////
    //Coherent State E - Creation and hashing
    /////////////////////////////////////////

    for (unsigned int two_iE = limiE1; two_iE <= limiE2; two_iE+=2){

      khint_t kE;
      coherentStateiE = 0. + 0.*I;
      HashTable_key_t keyE = {5,two_iE};
      kE=kh_put(HashTable, h, keyE, &retE);

      if ( retE == 1 ){
        check = check +1;
        coherentStateiE = CoherentStateI(two_iE, two_j4, two_j6, two_j7, two_j8,
                                         +(fE1), +(tE1),
                                         +(fE2), +(tE2),
                                         +(fE3), +(tE3),
                                         +(fE4), +(tE4),
                                         +1,+1,+1,+1);
        kh_value(h, kE) = coherentStateiE;
        kE=kh_get(HashTable, h, keyE);
        if( kh_val(h,kE) == 0 || kh_val(h,kE) != coherentStateiE){
          kh_val(h,kE) = coherentStateiE;
        }
      }
    }

    //////////////////// Write the hash table to disk ////////////////////

    kh_write(HashTable, h, pathRead);

    //////////////////// Free Memory ////////////////////

    void wig_temp_free();
    void wig_table_free();

    kh_destroy(HashTable, h);

    printf("\nSuccesfull creation of |%i %i %i %i %i %i %i %i %i %i| Coherent States Hash Table\n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10);
  }
  else{
    printf("\nWe have already computed |%i %i %i %i %i %i %i %i %i %i| Coherent States Hash Table\n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10);
  }
  config_destroy(cf);
  return 0;
}
