// gcc-7 -std=c99 -fopenmp -lgmp -lmpfr -lmpc -Lwigxjpf-1.5/lib -Iwigxjpf-1.5/inc -Iinc src/B4Hash.c -lm -lwigxjpf -lgsl -lconfig -o bin/B4Hash
#include "wigxjpf.h"            //To compute 3j,6j symbols
#include "khash.h"              //Hash Table
#include "Utilities.h"         //Boosters Functions
#include "B4Function.h"         //Boosters Functions
#include "Common.h"

//////////////////// Initialize Hash Table ////////////////////

ARRAY_TABLE_INIT(HashTable, 10*sizeof(int), double);

int main(int argc, char **argv){

  //////////////////// Initialize configuration file ////////////////////

  config_t cfg, *cf;
  const config_setting_t *Two_Spins;
  const config_setting_t *ImmirziPar;

  unsigned int two_j1Min, two_j2Min, two_j3Min, two_j4Min, two_j5Min, two_j6Min, two_j7Min, two_j8Min, two_j9Min, two_j10Min;

  cf = &cfg;
  config_init(cf);

  //////////////////// Check configuration file ////////////////////

  if (!config_read_file(cf, "../config/SL2Configurations.txt")){

      fprintf(stderr, "%s:%d - %s\n",
              config_error_file(cf),
              config_error_line(cf),
              config_error_text(cf));
      config_destroy(cf);
      return(EXIT_FAILURE);
  }

  //////////////////// Initialize minimum boundary spins ////////////////////

  char configTwo_Spins[200];
  sprintf(configTwo_Spins, "%s.Two_Spins",argv[1]);

  Two_Spins = config_lookup(cf, configTwo_Spins);
  two_j1Min = config_setting_get_int_elem(Two_Spins, 0);
  two_j2Min = config_setting_get_int_elem(Two_Spins, 1);
  two_j3Min = config_setting_get_int_elem(Two_Spins, 2);
  two_j4Min = config_setting_get_int_elem(Two_Spins, 3);
  two_j5Min = config_setting_get_int_elem(Two_Spins, 4);
  two_j6Min = config_setting_get_int_elem(Two_Spins, 5);
  two_j7Min = config_setting_get_int_elem(Two_Spins, 6);
  two_j8Min = config_setting_get_int_elem(Two_Spins, 7);
  two_j9Min = config_setting_get_int_elem(Two_Spins, 8);
  two_j10Min = config_setting_get_int_elem(Two_Spins, 9);

  //////////////////// Initialize Immirzi parameter ////////////////////

  char configImmirzi[200];
  sprintf(configImmirzi, "%s.Immirzi",argv[1]);
  ImmirziPar = config_lookup(cf, configImmirzi);
  float Immirzi = config_setting_get_float(ImmirziPar);
  float ImmirziSave =trunc(100 * Immirzi) / 100;

  char folderPath[200];
  char folderPath1[200];
  char folderPath2[200];
  char folderPath3[200];
  struct stat st = {0};

  //////////////////// Check data folders ////////////////////

  sprintf(folderPath, "../data/Immirzi_%.2f",ImmirziSave);
  sprintf(folderPath1, "../data/Immirzi_%.2f/HashTablesBooster",ImmirziSave);
  sprintf(folderPath2, "../data/Immirzi_%.2f/SL2CResults",ImmirziSave);
  sprintf(folderPath3, "../data/Immirzi_%.2f/HashTablesSL2C",ImmirziSave);

  if (stat(folderPath, &st) == -1){
    mkdir(folderPath, 0700);
    mkdir(folderPath1, 0700);
    mkdir(folderPath2, 0700);
    mkdir(folderPath3, 0700);
  }

  if (stat(folderPath1, &st) == -1){
    mkdir(folderPath1, 0700);
  }

  //////////////////// Rescaling parameters and Dl ////////////////////

  unsigned int two_N = atoi(argv[2]);
  unsigned int two_Dl = atoi(argv[3]);

  //////////////////// Parameters for Boosters ////////////////////

  unsigned int two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10;
  unsigned int two_k1, two_k2, two_k3, two_k4, two_k5, two_k6, two_k7, two_k8, two_k9, two_k10;
  float two_rho1, two_rho2, two_rho3, two_rho4, two_rho5, two_rho6, two_rho7, two_rho8, two_rho9, two_rho10;

  two_j1 = two_N*two_j1Min; two_j2 = two_N*two_j2Min; two_j3 = two_N*two_j3Min; two_j4 = two_N*two_j4Min; two_j5 = two_N*two_j5Min;
  two_j6 = two_N*two_j6Min; two_j7 = two_N*two_j7Min; two_j8 = two_N*two_j8Min; two_j9 = two_N*two_j9Min; two_j10 = two_N*two_j10Min;

  two_k1 = two_N*two_j1Min; two_k2 = two_N*two_j2Min; two_k3 = two_N*two_j3Min; two_k4 = two_N*two_j4Min; two_k5 = two_N*two_j5Min;
  two_k6 = two_N*two_j6Min; two_k7 = two_N*two_j7Min; two_k8 = two_N*two_j8Min; two_k9 = two_N*two_j9Min; two_k10 = two_N*two_j10Min;

  two_rho1=Immirzi*(float)two_j1; two_rho2=Immirzi*(float)two_j2; two_rho3=Immirzi*(float)two_j3; two_rho4=Immirzi*(float)two_j4;
  two_rho5=Immirzi*(float)two_j5; two_rho6=Immirzi*(float)two_j6; two_rho7=Immirzi*(float)two_j7; two_rho8=Immirzi*(float)two_j8;
  two_rho9=Immirzi*(float)two_j9; two_rho10=Immirzi*(float)two_j10;

  char pathRead[200];

  sprintf(pathRead, "../data/Immirzi_%.2f/HashTablesBooster/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.boost",
          ImmirziSave,two_j1, two_j2, two_j3, two_j4,two_j5, two_j6, two_j7, two_j8, two_j9, two_j10,two_Dl);

  if(file_exist (pathRead) == 0 ){

    khash_t(HashTable) *h = NULL;

    wig_table_init(2*1000, 3);
    #pragma omp parallel
    wig_thread_temp_init(2*1000);

    char pathRead1[200];

    //////////////////// Check if data already exists and load previopusly computed tables ////////////////////

    for ( int i = two_Dl;  i >= 0; i-=2){

      sprintf(pathRead1, "../data/Immirzi_%.2f/HashTablesBooster/%i.%i.%i.%i.%i.%i.%i.%i.%i.%i_%i.boost",
              ImmirziSave,two_j1, two_j2, two_j3, two_j4,two_j5, two_j6, two_j7, two_j8, two_j9, two_j10,i);

      if(file_exist (pathRead1) != 0 ){

        h = kh_load(HashTable, pathRead1);
        break;
      }
    }

    if ( h == NULL){

      h = kh_init(HashTable);
    }

    //////////////////////////////////////////
    //Booster Function A - Creation and hashing
    //////////////////////////////////////////

    int two_Ji1_minA=(int)max(abs(two_j2-two_j3),abs(two_j5-two_j4)); // Intertwiners range
    int two_Ji1_maxA=(int)min(two_j2+two_j3,two_j5+two_j4);
    int two_Ji1_boundA=two_Ji1_maxA-two_Ji1_minA;

    for (unsigned int two_l2 = two_j2 ; two_l2 <= two_j2+two_Dl; two_l2+=2){
      for (unsigned int two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
          for (unsigned int two_l4 = two_j4; two_l4 <= two_j4+two_Dl; two_l4+=2){

            int two_Ji2_minA=(int)max(abs(two_l2-two_l3),abs(two_j5-two_l4));
            int two_Ji2_maxA=(int)min(two_l2+two_l3,two_j5+two_l4);
            int two_Ji2_boundA=two_Ji2_maxA-two_Ji2_minA;

            HashTable_key_t keyCheckA = {two_j2, two_j3, two_j5, two_j4,
                                         two_l2,two_l3,two_j5,two_l4,
                                         two_Ji1_minA,two_Ji2_minA};

            if (kh_get(HashTable, h, keyCheckA) == kh_end(h)){ // Check we haven't already the function

              long double  **B4_moyA = B4Function(two_k2,  two_k3,  two_k5,  two_k4,
                                                  two_rho2,  two_rho3,  two_rho5,  two_rho4,
                                                  two_j2,  two_j3,  two_j5,  two_j4,
                                                  two_l2,  two_l3,  two_j5,  two_l4
                                                 ); // Compute the function for every intertwiner

              for(int two_Ji1A=0;two_Ji1A<=two_Ji1_boundA;two_Ji1A+=2){ // Save all intertwiners combinations
                  for(int two_Ji2A=0;two_Ji2A<=two_Ji2_boundA;two_Ji2A+=2){

                      int retA;
                      HashTable_key_t keyA = {two_j2, two_j3, two_j5, two_j4,
                                              two_l2,two_l3,two_j5,two_l4,
                                              two_Ji1A+two_Ji1_minA,two_Ji2A+two_Ji2_minA};
                      khint_t kA = kh_put(HashTable, h, keyA, &retA);

                      int Ji1A = two_Ji1A/2;
                      int Ji2A = two_Ji2A/2;
                      double valB4A = (double)B4_moyA[Ji2A][Ji1A];

                      kh_val(h,kA) = valB4A;
                      kA=kh_get(HashTable, h, keyA);

                      if( kh_val(h,kA) != valB4A){
                          kh_val(h,kA) = valB4A;
                      }
                  }
                }
            //free (B4_moyA);
            }
          }
        }
      }

      //////////////////////////////////////////
      //Booster Function B - Creation and hashing
      //////////////////////////////////////////

      int two_Ji1_minB=(int)max(abs(two_j10-two_j7),abs(two_j1-two_j2));
      int two_Ji1_maxB=(int)min(two_j10+two_j7,two_j1+two_j2);
      int two_Ji1_boundB=two_Ji1_maxB-two_Ji1_minB;

      for (unsigned int two_l7 = two_j7 ; two_l7 <= two_j7+two_Dl; two_l7+=2){
        for (unsigned int two_l1 = two_j1; two_l1 <= two_j1+two_Dl; two_l1+=2){
          for (unsigned int two_l2 = two_j2; two_l2 <= two_j2+two_Dl; two_l2+=2){

              int two_Ji2_minB=(int)max(abs(two_j10-two_l7),abs(two_l1-two_l2));
              int two_Ji2_maxB=(int)min(two_j10+two_l7,two_l1+two_l2);
              int two_Ji2_boundB=two_Ji2_maxB-two_Ji2_minB;

              HashTable_key_t keyCheckB = {two_j10, two_j7, two_j1, two_j2,
                                           two_j10,two_l7,two_j1,two_l2,
                                           two_Ji1_minB,two_Ji2_minB};

              if (kh_get(HashTable, h, keyCheckB) == kh_end(h)){

                long double  **B4_moyB = B4Function( two_k10,  two_k7,  two_k1,  two_k2,
                                                    two_rho10,  two_rho7,  two_rho1,  two_rho2,
                                                    two_j10,  two_j7,  two_j1,  two_j2,
                                                    two_j10,  two_l7,  two_l1,  two_l2
                                                    );

                for(int two_Ji1B=0;two_Ji1B<=two_Ji1_boundB;two_Ji1B+=2){
                    for(int two_Ji2B=0;two_Ji2B<=two_Ji2_boundB;two_Ji2B+=2){

                      int retB;
                      HashTable_key_t keyB = {two_j10, two_j7, two_j1, two_j2,
                                              two_j10,two_l7,two_l1,two_l2,
                                              two_Ji1B+two_Ji1_minB,two_Ji2B+two_Ji2_minB};
                      khint_t kB = kh_put(HashTable, h, keyB, &retB);

                      int Ji1B = two_Ji1B/2;
                      int Ji2B = two_Ji2B/2;
                      double valB4B = (double)B4_moyB[Ji2B][Ji1B];

                      kh_val(h,kB) = valB4B;
                      kB=kh_get(HashTable, h, keyB);
                      if( kh_val(h,kB) == 0 || kh_val(h,kB) != valB4B){
                          kh_val(h,kB) = valB4B;
                      }
                    }
                }
              //free (B4_moyB);
          }
        }
      }
    }

    //////////////////////////////////////////
    //Booster Function C - Creation and hashing
    //////////////////////////////////////////

    int two_Ji1_minC=(int)max(abs(two_j9-two_j8),abs(two_j3-two_j1));
    int two_Ji1_maxC=(int)min(two_j9+two_j8,two_j3+two_j1);
    int two_Ji1_boundC=two_Ji1_maxC-two_Ji1_minC;

    for (unsigned int two_l8 = two_j8 ; two_l8 <= two_j8+two_Dl; two_l8+=2){
      for (unsigned int two_l3 = two_j3; two_l3 <= two_j3+two_Dl; two_l3+=2){
        for (unsigned int two_l1 = two_j1; two_l1 <= two_j1+two_Dl; two_l1+=2){

          int two_Ji2_minC=(int)max(abs(two_j9-two_l8),abs(two_l3-two_l1));
          int two_Ji2_maxC=(int)min(two_j9+two_l8,two_l3+two_l1);
          int two_Ji2_boundC=two_Ji2_maxC-two_Ji2_minC;

          HashTable_key_t keyCheckC = {two_j9,two_j8,two_j3,two_j1,
                                       two_j9,two_l8,two_l3,two_l1,
                                       two_Ji1_minC,two_Ji2_minC};

          if (kh_get(HashTable, h, keyCheckC) == kh_end(h)){

            long double  **B4_moyC = B4Function( two_k9,  two_k8,  two_k3,  two_k1,
                                                two_rho9,  two_rho8,  two_rho3,  two_rho1,
                                                two_j9,  two_j8,  two_j3,  two_j1,
                                                two_j9,  two_l8,  two_l3,  two_l1
                                                );

            for(int two_Ji1C=0;two_Ji1C<=two_Ji1_boundC;two_Ji1C+=2){
                for(int two_Ji2C=0;two_Ji2C<=two_Ji2_boundC;two_Ji2C+=2){

                  int retC;
                  HashTable_key_t keyC = {two_j9,two_j8,two_j3,two_j1,
                                          two_j9,two_l8,two_l3,two_l1,
                                          two_Ji1C+two_Ji1_minC,two_Ji2C+two_Ji2_minC};
                  khint_t kC = kh_put(HashTable, h, keyC, &retC);

                  int Ji1C = two_Ji1C/2;
                  int Ji2C = two_Ji2C/2;
                  double valB4C = (double)B4_moyC[Ji2C][Ji1C];

                  kh_val(h,kC) = valB4C;
                  kC=kh_get(HashTable, h, keyC);
                  if( kh_val(h,kC) == 0 || kh_val(h,kC) != valB4C){
                      kh_val(h,kC) = valB4C;
                  }
                }
            }
          //free (B4_moyC);
          }
        }
      }
    }

    //////////////////////////////////////////
    //Booster Function E - Creation and hashing
    //////////////////////////////////////////

    int two_Ji1_minD=(int)max(abs(two_j4-two_j6),abs(two_j7-two_j8));
    int two_Ji1_maxD=(int)min(two_j4+two_j6,two_j7+two_j8);
    int two_Ji1_boundD=two_Ji1_maxD-two_Ji1_minD;

    for (unsigned int two_l4 = two_j4 ; two_l4 <= two_j4+two_Dl; two_l4+=2){
      for (unsigned int two_l7 = two_j7; two_l7 <= two_j7+two_Dl; two_l7+=2){
        for (unsigned int two_l8 = two_j8; two_l8 <= two_j8+two_Dl; two_l8+=2){

          int two_Ji2_minD=(int)max(abs(two_l4-two_j6),abs(two_l7-two_l8));
          int two_Ji2_maxD=(int)min(two_l4+two_j6,two_l7+two_l8);
          int two_Ji2_boundD=two_Ji2_maxD-two_Ji2_minD;

          HashTable_key_t keyCheckD = {two_j4,two_j6,two_j7,two_j8,
                                       two_l4,two_j6,two_l7,two_l8,
                                       two_Ji1_minD,two_Ji2_minD};

          if (kh_get(HashTable, h, keyCheckD) == kh_end(h)){

            long double  **B4_moyD = B4Function( two_k4,  two_k6,  two_k7,  two_k8,
                                                two_rho4, two_rho6, two_rho7, two_rho8,
                                                two_j4, two_j6, two_j7, two_j8,
                                                two_l4, two_j6, two_l7, two_l8
                                                );

            for(int two_Ji1D=0;two_Ji1D<=two_Ji1_boundD;two_Ji1D+=2){
                for(int two_Ji2D=0;two_Ji2D<=two_Ji2_boundD;two_Ji2D+=2){

                  int retD;
                  HashTable_key_t keyD = {two_j4,two_j6,two_j7,two_j8,
                                          two_l4,two_j6,two_l7,two_l8,
                                          two_Ji1D+two_Ji1_minD,two_Ji2D+two_Ji2_minD};
                  khint_t kD = kh_put(HashTable, h, keyD, &retD);

                  int Ji1D = two_Ji1D/2;
                  int Ji2D = two_Ji2D/2;
                  double valB4D = (double)B4_moyD[Ji2D][Ji1D];

                  kh_val(h,kD) = valB4D;
                  kD=kh_get(HashTable, h, keyD);
                  if( kh_val(h,kD) == 0 || kh_val(h,kD) != valB4D){
                      kh_val(h,kD) = valB4D;
                  }
                }
            }
          //free (B4_moyD);
          }
        }
      }
    }

    //////////////////// Write the hash table to disk ////////////////////

    printf("\nHash Entries for Boosters: %d \n", kh_size(h));
    kh_write(HashTable, h, pathRead);

    //////////////////// Free Memory ////////////////////

    void wig_temp_free();
    void wig_table_free();

    kh_destroy(HashTable, h);
  }
  else{
    printf("\nWe have already computed |%i %i %i %i %i %i %i %i %i %i| %i | Booster Hash Table \n\n",
          two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9, two_j10, two_Dl);
  }

  config_destroy(cf);
  return 0;
}
