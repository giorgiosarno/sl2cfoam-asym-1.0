#ifndef __COHERENTSTATES_H__
#define __COHERENTSTATES_H__

#include "Common.h"

//////////////////// Coherent Tetrahedron at intertwiner I ////////////////////

double complex CoherentStateI(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int,
                              double, double, double, double, double, double, double, double,
                              int, int, int, int);

//////////////////// SU(2) Wigner D Matrix ////////////////////

double complex WignerD (unsigned int ,int ,double ,double, int );

//////////////////// Gaussian intertwiner cut for Euclidean and Lorentzian 4-simplices ////////////////////

void CoherentGaussianCut( const kh_HashTableCoherent_t *, char *, 
                          unsigned int , unsigned int , unsigned int , unsigned int ,
                          unsigned int, unsigned int, unsigned int,
                          unsigned int );


#endif /*__COHERENTSTATES_H__*/
