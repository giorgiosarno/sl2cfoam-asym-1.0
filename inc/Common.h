#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <tgmath.h>
#include <gmp.h>                  //Precision Library
#include <mpfr.h>                 //Precision Library
#include <mpc.h>                //Precision Library
#include <omp.h>                  // Parallelization utilities
#include <unistd.h>               // To check if a file already exists
#include <sys/stat.h>             // To check if a file already exists
#include <libconfig.h>            //Configuration file library
#include <gsl/gsl_statistics_double.h>   //GSL
#include <gsl/gsl_sf_gamma.h>   //GSL complex gamma function
#include <gsl/gsl_sf_result.h>

#include "khash.h"                //Hash Table

#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define d(two_a) (two_a+1)

#define m_pi 3.14159265358979323846

//////////////////// Initialize Hash Tables ////////////////////

ARRAY_TABLE_INIT(HashTableJ6, 6*sizeof(int), double); // 24 bytes keys (6 int * 4 bytes) with double values.
ARRAY_TABLE_INIT(HashTableCoherent, 2*sizeof(int), double complex); // 8 bytes keys (2 int * 4 bytes) with double complex values.
ARRAY_TABLE_INIT(HashTableBooster, 10*sizeof(int), double); // 24 bytes keys (6 int * 4 bytes) with double values.
ARRAY_TABLE_INIT(HashTableMagnetic, 6*sizeof(int), double complex); // 24 bytes keys (6 int * 4 bytes) with double complex values.


#endif/*__COMMON_H__*/
