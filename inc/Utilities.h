#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include "Common.h"

//////////////////// Multiplication of complex values as MPFR variables ////////////////////

double complex MPFRComplexMultiplication(double complex , double complex );

//////////////////// File Check ////////////////////

int file_exist ( char * );

//////////////////// Compensated Summation Alghoritm ////////////////////

void CompensatedSummationMPFR (double complex *, double complex *, mpfr_t , mpfr_t );

//////////////////// Hash Keys for L Sets for equal boosted boundary tetrahedra ////////////////////

#define B4_CELLS_INT 500
#define B4_CELLS_L 1000

int B4_Hash_Symmetric (unsigned int , unsigned int , unsigned int , unsigned int ,
  unsigned int , unsigned int , unsigned int , unsigned int );

//////////////////// Hash Keys for L Sets for non equal boosted boundary tetrahedra ////////////////////
//TODO This Hash function has to be checked. Overflow?

int B4_Hash_ASymmetric (unsigned int , unsigned int , unsigned int , unsigned int,
                        unsigned int , unsigned int , unsigned int , unsigned int );

#endif /*__UTILITIES_H__*/
