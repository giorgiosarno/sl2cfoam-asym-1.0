#include <stdio.h>
#include <stdlib.h>
#include "JSymbols.h"

#define W6U32 uint32_t

#define COLLECT_NEGATIVE(two_j1,two_j2,two_j3) do {     \
collect_sign |= (two_j1) | (two_j2) | (two_j3);     \
} while (0)

#define COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1,two_j2,two_j3) do {        \
collect_sign |= (((two_j2) + (two_j3)) - (two_j1));                 \
collect_sign |= ((two_j1) - ((two_j2) - (two_j3)));                 \
collect_sign |= ((two_j1) - ((two_j3) - (two_j2)));                 \
} while (0)

void WIGNER6J_REGGE_CANONICALISE(char **ret, uint32_t two_j1, uint32_t two_j2, uint32_t two_j3,
                                                    uint32_t two_j4, uint32_t two_j5, uint32_t two_j6){

  W6U32 b1 = (two_j1 + two_j2 + two_j3);
  W6U32 b2 = (two_j1 + two_j5 + two_j6);
  W6U32 b3 = (two_j4 + two_j2 + two_j6);
  W6U32 b4 = (two_j4 + two_j5 + two_j3);

  /* Check trivial-0 */
  W6U32 collect_sign = 0;
  W6U32 collect_odd = 0;

  collect_odd = b1 | b2 | b3 | b4;

  COLLECT_NEGATIVE(two_j1, two_j2, two_j3);
  COLLECT_NEGATIVE(two_j4, two_j5, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1, two_j2, two_j3);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j1, two_j5, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j4, two_j2, two_j6);
  COLLECT_TRIANGLE_TRIVIAL_ZERO(two_j4, two_j5, two_j3);

  if ((collect_sign & (1 << (sizeof (int) * 8 - 1))) |
      (collect_odd & 1)){
    for (int i=0; i<=5; i++){
        (*ret)[i]=0;
    }
  }

  /* Check trivial-0 end */

  # define SHIFT_DOWN_1(a)  ((a) >> 1)

  b1 = SHIFT_DOWN_1(b1);
  b2 = SHIFT_DOWN_1(b2);
  b3 = SHIFT_DOWN_1(b3);
  b4 = SHIFT_DOWN_1(b4);

  W6U32 a1 = SHIFT_DOWN_1(two_j1 + two_j2 + two_j4 + two_j5);
  W6U32 a2 = SHIFT_DOWN_1(two_j1 + two_j3 + two_j4 + two_j6);
  W6U32 a3 = SHIFT_DOWN_1(two_j2 + two_j3 + two_j5 + two_j6);

  #define SWAP_TO_FIRST_LARGER(tmptype,a,b) do {		\
  tmptype __tmp_a = a;				\
  tmptype __tmp_b = b;				\
  a = (__tmp_a > __tmp_b) ? __tmp_a : __tmp_b;	\
  b = (__tmp_a < __tmp_b) ? __tmp_a : __tmp_b;	\
  } while (0)

  SWAP_TO_FIRST_LARGER(W6U32, a1, a2);
  SWAP_TO_FIRST_LARGER(W6U32, a2, a3);
  SWAP_TO_FIRST_LARGER(W6U32, a1, a2);

  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);
  SWAP_TO_FIRST_LARGER(W6U32, b2, b3);
  SWAP_TO_FIRST_LARGER(W6U32, b3, b4);
  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);
  SWAP_TO_FIRST_LARGER(W6U32, b2, b3);
  SWAP_TO_FIRST_LARGER(W6U32, b1, b2);

  // We now have a1 >= a2 >= a3, and b1 >= b2 >= b3 >= b4

  W6U32 S32 = a3 - b1;
  W6U32 B32 = a3 - b2;
  W6U32 T32 = a3 - b3;
  W6U32 X32 = a3 - b4;
  W6U32 L32 = a2 - b4;
  W6U32 E32 = a1 - b4;

  (*ret)[0]=E32;     (*ret)[1]=L32;     (*ret)[2]=X32;
  (*ret)[3]=T32;     (*ret)[4]=B32;     (*ret)[5]=S32;

}

#undef W6U32

#undef SWAP_TO_FIRST_LARGER

double J15Symbol ( const kh_HashTableJ6_t *h,
            char **A, char **B, char **C, char **D, char **E,
            unsigned int two_l1, unsigned int two_l2, unsigned int two_l3, unsigned int two_l4, unsigned int two_j5,
            unsigned int two_j6, unsigned int two_l7, unsigned int two_l8, unsigned int two_j9, unsigned int two_j10,
            unsigned int two_i1, unsigned int two_i2, unsigned int two_i3, unsigned int two_i4, unsigned int two_i5,
            unsigned int two_k1, unsigned int two_k2, unsigned int two_k3, unsigned int two_k5){


  double CutOff = pow(10,-40);

  ////////////////////////////
  //Recover 6j and Compute 15j
  ////////////////////////////

  double val6jA,val6jB,val6jC,val6jD,val6jE;

  khiter_t kA,kB,kC,kD,kE;

  WIGNER6J_REGGE_CANONICALISE(A, two_k1, two_k3, two_k2, two_l1, two_l2, two_l3);
  HashTableJ6_key_t keyA = {(*A)[0],(*A)[1],(*A)[2],(*A)[3],(*A)[4],(*A)[5]};
  kA = kh_get(HashTableJ6, h, keyA);
  val6jA = kh_value(h, kA);

  if (fabs(val6jA) < CutOff) return 0;

  WIGNER6J_REGGE_CANONICALISE(B, two_k1, two_i4, two_k5, two_j6, two_l4, two_j5);
  HashTableJ6_key_t keyB = {(*B)[0],(*B)[1],(*B)[2],(*B)[3],(*B)[4],(*B)[5]};
  kB = kh_get(HashTableJ6, h, keyB);
  val6jB = kh_value(h, kB);

  if (fabs(val6jB) < CutOff) return 0;

  unsigned int two_limx1 = max(max(abs(two_k2-two_k5),abs(two_j10-two_l8)),abs(two_k3-two_i4));
  unsigned int two_limx2 = min(min(two_k2+two_k5,two_j10+two_l8),two_k3+two_i4);

  ////////////////////////////
  //9j via 6j summation
  ////////////////////////////

  double val9j = 0.0;

  for( unsigned int two_x = two_limx1; two_x <= two_limx2; two_x+=2){

    WIGNER6J_REGGE_CANONICALISE(C, two_k2,two_j10,two_l7,two_l8,two_k5,two_x);
    HashTableJ6_key_t keyC = {(*C)[0],(*C)[1],(*C)[2],(*C)[3],(*C)[4],(*C)[5]};
    kC = kh_get(HashTableJ6, h, keyC);
    val6jC = kh_value(h, kC);
    if (fabs(val6jC) < CutOff) continue;

    WIGNER6J_REGGE_CANONICALISE(D, two_k3,two_j9,two_l8,two_j10,two_x,two_i4);
    HashTableJ6_key_t keyD= {(*D)[0],(*D)[1],(*D)[2],(*D)[3],(*D)[4],(*D)[5]};
    kD = kh_get(HashTableJ6, h, keyD);
    val6jD = kh_value(h, kD);
    if (fabs(val6jD) < CutOff) continue;

    WIGNER6J_REGGE_CANONICALISE(E, two_k1,two_i4,two_k5,two_x,two_k2,two_k3);
    HashTableJ6_key_t keyE = {(*E)[0],(*E)[1],(*E)[2],(*E)[3],(*E)[4],(*E)[5]};
    kE = kh_get(HashTableJ6, h, keyE);
    val6jE = kh_value(h, kE);
    if (fabs(val6jE) < CutOff) continue;

    val9j += pow(-1,two_x)*(two_x+1.0)*val6jC*val6jD*val6jE;
  }
  //printf ("%17g \n", val9j);
  double val15j = pow(-1,(two_l4+two_l3+two_k1)-(two_k2+two_k3+two_i4+two_k5)/2)
           *val6jA*val6jB*val9j;

  return val15j;
}
