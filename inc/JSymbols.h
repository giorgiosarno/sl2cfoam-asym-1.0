#ifndef __J6CANONICALIZATION_H__
#define __J6CANONICALIZATION__

#include "Common.h"

//////////////////// Canonicalization function for 6j symbols
//////////////////// From spins to keys

typedef __uint32_t  uint32_t;

void WIGNER6J_REGGE_CANONICALISE(char **ret, uint32_t two_j1, uint32_t two_j2, uint32_t two_j3,
                                             uint32_t two_j4, uint32_t two_j5, uint32_t two_j6);

//////////////////// 15J symbol Function ////////////////////


double J15Symbol (  const kh_HashTableJ6_t *,
                    char **, char **, char **, char **, char **,
                    unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int , unsigned int , unsigned int ,
                    unsigned int , unsigned int , unsigned int , unsigned int
                  );
#endif /*__J6CANONICALIZATION_H__*/
