#include "JSymbols.h"   //Canonicalization of 6j symbols
#include "Utilities.h"

int file_exist (char *filename){

    struct stat   buffer;
    return (stat (filename, &buffer) == 0);
}

double complex MPFRComplexMultiplication(double complex coherentA, double complex coherentB){

  mpfr_t  statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
          statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
          statesMPFR1, statesMPFR2;

  mpfr_rnd_t GMP_RNDN;

  mpfr_init_set_d (statesMPFR_A_Re, creal(coherentA), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_B_Re, creal(coherentB), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_A_Im, cimag(coherentA), GMP_RNDN);
  mpfr_init_set_d (statesMPFR_B_Im, cimag(coherentB), GMP_RNDN);

  mpfr_inits (statesMPFR_Re1, statesMPFR_Re2, statesMPFR_Im1, statesMPFR_Im2, NULL);

  mpfr_mul (statesMPFR_Re1, statesMPFR_A_Re, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Re2, statesMPFR_A_Im, statesMPFR_B_Im, GMP_RNDN);
  mpfr_mul (statesMPFR_Im1, statesMPFR_A_Im, statesMPFR_B_Re, GMP_RNDN);
  mpfr_mul (statesMPFR_Im2, statesMPFR_A_Re, statesMPFR_B_Im, GMP_RNDN);

  mpfr_inits (statesMPFR1,statesMPFR2, NULL);
  mpfr_sub  (statesMPFR1, statesMPFR_Re1, statesMPFR_Re2, GMP_RNDN);
  mpfr_add  (statesMPFR2, statesMPFR_Im1, statesMPFR_Im2, GMP_RNDN);

  double complex result = mpfr_get_d (statesMPFR1, GMP_RNDN) + I*mpfr_get_d (statesMPFR2, GMP_RNDN);

  mpfr_clears ( statesMPFR_A_Re, statesMPFR_B_Re, statesMPFR_A_Im, statesMPFR_B_Im,
                statesMPFR_Re1,statesMPFR_Re2, statesMPFR_Im1,statesMPFR_Im2,
                statesMPFR1, statesMPFR2, NULL );

  return result;
}

void CompensatedSummationMPFR (double complex *err, double complex *SU2Value, mpfr_t SU2ValueMPFR_Re, mpfr_t SU2ValueMPFR_Im ){

  mpfr_rnd_t GMP_RNDN;

  double complex Aux1,Aux2,Aux3;

  Aux1 = mpfr_get_d (SU2ValueMPFR_Re, GMP_RNDN) + mpfr_get_d (SU2ValueMPFR_Im, GMP_RNDN)*I;
  Aux2 = *SU2Value + Aux1;
  Aux3 = (Aux2 - *SU2Value) - Aux1;
  *err = Aux3;
  *SU2Value =  Aux2;

}


int B4_Hash_Symmetric(unsigned int two_j1, unsigned int two_j2, unsigned int two_j3, unsigned int two_j4,
                      unsigned int two_l1, unsigned int two_l2, unsigned int two_l3, unsigned int two_l4) {

    int hash = ((two_l1-two_j1) + 7*(two_l2-two_j2)
                + 8*9*(two_l3-two_j3) + 10*11*12*(two_l4-two_j4)) ;

    return hash % B4_CELLS_L;

}
//TODO This function has to be checked with a proper Configuration
// I can not assure that it is safe, probably we'll overflow.
//Get a configuration and check
int B4_Hash_ASymmetric(unsigned int two_j1, unsigned int two_j2, unsigned int two_j3, unsigned int two_j4,
                       unsigned int two_l1, unsigned int two_l2, unsigned int two_l3, unsigned int two_l4) {

  int two_jmin = min(min(two_j1,two_j2), min(two_j3,two_j4));
  int hash = ((two_l1-two_jmin) + 3*(two_l2-two_jmin)
              + 4*5*(two_l3-two_jmin) + 5*6*7*(two_l4-two_jmin)) ;

    return hash % B4_CELLS_L;

}
