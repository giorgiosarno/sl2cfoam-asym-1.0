#ifndef __B4FUNCTION_H__
#define __B4FUNCTION_H__

#include "Common.h"

float maxF(float e,float f);

float minF(float e,float f);

long double fact(float j);

/// Functions to compute Collet's B4 formula///

void f(mpfr_t f_res,int l,float J,float j,int s,float p);

void alpha(mpc_t alpha_res,int l,float J,float rho,
          float j2,float j1,int s2,int s1,float p,int m);

void alpha_0(mpfr_t alpha_res,int l,float J,float j2,
            float j1,int s2,int s1,float p,int m);

void sigma(mpfr_t sigma_res,int l,float J,float j2,
          float j1,int s2,int s1,float p,float ms);

void sqrtj(mpfr_t sqrtj_res,int l,float J,float j,float p);

void Y(mpc_t Y_res,int l,float J,float rho,
      float j2,float j1,float p,int m);

void Z(mpfr_t Z_res,int l,float J,
      float j2,float j1,float p,int m);

void W(mpfr_t W_res,int l,float J,
      float j2,float j1,float p,float ms);

long double complex D(mpc_t Ym[],mpc_t Yn[],int l,float J,float rho,
                      float j2,float j1,float p,long double X);

long double complex D_0(mpfr_t Zm[],mpfr_t Zn[],mpfr_t W[],int l,float J,
                        float j2,float j1,float p,long double X);


long double mesure_x(long double X);

/// Integration ///

void I4_Interval(long double complex *I4_inf,long double complex *I4_sup,int N,
                 long double complex *D1,long double complex *D2,long double complex *D3,long double complex *D4);

/// Intertwiners ///

long double intertwiner_j(float Ji,long double trois_j_part1,long double trois_j_part2,
                          float m1,float m2,float m3,float m4);

/// Complex Gamma to rephase ;///

long double complex ComplexGamma(double complex z);


long double complex dPhase (float j, float l, float rho);

/// Main functions for B4 ///

long double complex B4_Interval(long double complex *B4_inf,long double complex *B4_sup,
                                 int Ji2_int,float Ji2_min,int Ji1_int,float Ji1_min,
                                 float j1,float j2,float j3,float j4,
                                 long double ***trois_j2_part1,long double ***trois_j2_part2,
                                 long double ***trois_j1_part1,long double ***trois_j1_part2,
                                 long double complex ***I4_inf,long double complex ***I4_sup);

long double  **B4Function ( int two_k1,  int two_k2,  int two_k3, int two_k4,
                           float two_rho1, float two_rho2,float two_rho3,float two_rho4,
                           int two_j1,  int two_j2,  int two_j3,  int two_j4,
                           int two_l1,  int two_l2,  int two_l3,  int two_l4
                         );



#endif/*__B4FUNCTION_H__*/
