#include "wigxjpf.h"
#include "CoherentStatesFunctions.h"

double complex CoherentStateI(unsigned int two_i, unsigned int two_j1, unsigned int two_j2,unsigned int two_j3,unsigned int two_j4,
                              double f1, double t1, double f2, double t2, double f3, double t3, double f4, double t4,
                              int sign1, int sign2, int sign3, int sign4 ){

    double complex wignerD1;
    double complex wignerD2;
    double complex wignerD3;
    double complex wignerD4;

    double complex coherentStatei ;
    double J4symbol;

    coherentStatei = 0+0*I ;

    for ( int two_m1 = -(int)two_j1; two_m1 <= (int)two_j1; two_m1+=2){
      for ( int two_m2 = max(-(int)two_i-(int)two_m1,-(int)two_j2); two_m2 <= min((int)two_i-(int)two_m1, (int)two_j2); two_m2 +=2){
        for ( int two_m3 = max(-(int)two_j4-(int)two_m1-(int)two_m2,-(int)two_j3);
                  two_m3 <= min ((int)two_j4-(int)two_m1-(int)two_m2, (int)two_j3); two_m3 +=2){

          wignerD1 = WignerD (two_j1, two_m1, f1, t1, sign1);
          wignerD2 = WignerD (two_j2, two_m2, f2, t2, sign2);
          wignerD3 = WignerD (two_j3, two_m3, f3, t3, sign3);
          wignerD4 = WignerD (two_j4, -two_m1-two_m2-two_m3, f4, t4, sign4);

          J4symbol = pow(-1,(float)two_i/2-(-(float)two_m1-(float)two_m2)/2)
          *wig3jj(two_j1,two_j2,two_i,two_m1,two_m2,-two_m1-two_m2)
          *wig3jj(two_j3,two_j4,two_i,two_m3,-two_m1-two_m2-two_m3,two_m1+two_m2);

          coherentStatei += J4symbol*wignerD1*wignerD2*wignerD3*wignerD4;
        }
      }
    }
  return(coherentStatei);
}



double complex WignerD (unsigned int two_j, int two_m, double f1, double t1, int sign){

    mpz_t rop1;
    mpz_t rop2;
    mpz_t rop3;

    double j = (float)two_j/2;
    double m = (float)two_m/2;
    unsigned long int op1,op2,op3;

    mpfr_rnd_t  rnd = GMP_RNDN; // MpFrC is a dummy class holding the static members of mpfr_class

    mpz_init ( rop1 ); //Initialize integer variables for gmp
    mpz_init ( rop2 );
    mpz_init ( rop3 );

    op1 = 2*j;    op2 = j+m;    op3 = j-m;

    mpz_fac_ui ( rop1, op1 ); //Factorials
    mpz_fac_ui ( rop2, op2 );
    mpz_fac_ui ( rop3, op3 );

    mpz_div(rop1,rop1,rop2);
    mpz_div(rop1,rop1,rop3);

    mpfr_t sqf; //inizialize float mpfr variables to account of the square root of factorials
    mpfr_init ( sqf );

    mpfr_set_z(sqf,rop1,GMP_RNDN );
    mpfr_sqrt(sqf,sqf,GMP_RNDN ); // compute sqrt

    mpz_clear ( rop1 ); //Clear mpz variables
    mpz_clear ( rop2 );
    mpz_clear ( rop3 );

    /*double result;
     result = mpfr_get_d(sqf, GMP_RNDN);
     printf ( " d = %f\n", result );*/

    mpfr_t th1;
    mpfr_t c1;
    mpfr_t s1;

    t1= t1/2; //Theta Angle for the D small. Divided by two.

    mpfr_init_set_d (th1, t1, GMP_RNDN); // set up a mpfr variable th1 with the value of the half angle

    mpfr_init ( s1 );
    mpfr_init ( c1 );

    mpfr_cos(c1, th1, GMP_RNDN); // calculata cosine and sine of t1
    mpfr_sin(s1, th1, GMP_RNDN );

    if (sign == -1)
    {
        mpfr_mul_d(s1, s1, -1, GMP_RNDN );
    }

    /*result = mpfr_get_d(s1, GMP_RNDN);
     printf ( " Sin = %f\n", result );*/

    mpfr_t result1;
    mpfr_t result2;

    mpfr_init ( result1 );
    mpfr_init ( result2 );

    if (sign == -1)
    {
        mpfr_pow_ui (result1, c1, j-m, GMP_RNDN);
    }
    else
    {
        mpfr_pow_ui (result1, c1, j+m, GMP_RNDN); // Exp of cos
    }
    /*result = mpfr_get_d(result1, GMP_RNDN);
     printf ( " Cos = %f\n", result );*/

    if (sign == -1)
    {
        mpfr_pow_ui (result2, s1, j+m, GMP_RNDN);
    }
    else
    {
        mpfr_pow_ui (result2, s1, j-m, GMP_RNDN);
    }

    mpfr_clear ( th1 );
    mpfr_clear ( c1 );
    mpfr_clear ( s1 );

    /*result = mpfr_get_d(result2, GMP_RNDN);
     printf ( " Sin = %f\n", result );*/

    mpfr_t result3;
    mpfr_init ( result3 );

    mpfr_mul(result3, result1, result2, GMP_RNDN ); // Create the D small function
    mpfr_mul(result3, result3, sqf, GMP_RNDN );

    double result = mpfr_get_d(result3, GMP_RNDN);
    /*printf ( " d = %f\n", result );*/

    mpfr_clear ( sqf );
    mpfr_clear ( result1 );
    mpfr_clear ( result2 );
    mpfr_clear ( result3 );

    double complex wignerD;

    if (sign == -1)
    {
        wignerD= cexp(-I*m*(f1))*result*cexp(I*j*(-f1));
    }
    else
    {
        wignerD= cexp(-I*m*(f1))*result*cexp(-I*j*(-f1));
    }

    //printf("%.15f %.15fi \n",creal(wignerD),cimag(wignerD));

    return wignerD;

}

void CoherentGaussianCut ( const kh_HashTableCoherent_t *h1, char *range,
                           unsigned int two_j5, unsigned int two_j6, unsigned int two_j9, unsigned int two_j10,
                           unsigned int two_N, unsigned int cutoff_gaussian, unsigned int number_tetra,
                           unsigned int config_type){

  unsigned int two_limi_1, two_limi_2;

  two_limi_1 = max(abs(two_j5-two_j6),abs(two_j10-two_j9));
  two_limi_2 = min(two_j5+two_j6,two_j10+two_j9);

  if ( cutoff_gaussian == 0 && config_type == 0 || two_N < 8 && config_type == 0 ||
       cutoff_gaussian == 0 && config_type == 1 || two_N < 3 && config_type == 1    ){
    range[0] = two_limi_1;
    range[1] = two_limi_2;
  }
  else
  {

    khint_t cGauss;
    unsigned int two_range_i = two_limi_2 - two_limi_1;
    double dataGauss[two_range_i];

    #pragma omp parallel for
    for(unsigned int two_i = two_limi_1; two_i <= two_limi_2; two_i+=2){

              HashTableCoherent_key_t cohGauss = {number_tetra, two_i};
              cGauss = kh_get(HashTableCoherent, h1, cohGauss);
              dataGauss[two_i] = cabs(kh_value(h1, cGauss));
    }

    double standardDeviation =  gsl_stats_sd (dataGauss, 2, two_range_i/2);
    double standardDeviationErr = standardDeviation / cutoff_gaussian;

    //printf ("%17g",standardDeviationErr);
    //exit(0);

    for(unsigned int two_i = two_limi_1; two_i <= two_limi_2; two_i+=2){

      if (dataGauss[two_i] > standardDeviationErr){

        range[0] = two_i;
        break;
      }
    }

    for(unsigned int two_i = range[0]; two_i <= two_limi_2; two_i+=2){
      if (standardDeviationErr > dataGauss[two_i] ){
        range[1] = two_i-2;
        break;
      }
    }
  }
}
