## Numerical methods for computing the asymptotic limit of the EPRL spin foam vertex 

This C program computes the asymptotic limit for a single 4-simplex vertex amplitude of the EPRL spin foam theory. 

It is an application of the code https://bitbucket.org/giorgiosarno/sl2cfoam-1.0/ described in our previous work "Numerical methods for EPRL spin foam transition amplitudes and Lorentzian recoupling theory" that can be found at https://arxiv.org/abs/1807.03066. 
We refer to this work for all the technical details concerning the code. For relevant background works see also https://arxiv.org/abs/1609.01632 and https://arxiv.org/abs/1708.01727

There are some differences respect to the sl2cfoam library, to optimize the computation of a coherent vertex. These are described in the companion paper: 

In the data folder we provide some of the data that we have. We do not provide hash tables for {6j} symbols because they are too heavy.
In the notebooks folder we also provide Mathematica notebooks to compute the hessian of the amplitude as well as notebooks to reconstruct geometrical 4-simplices, euclidean and lorentzian.

This code has been developed by Giorgio Sarno and Pietro Donà with the collaboration of Marco Fanizza, François Collet and Francesco Gozzini.

### Compiling

Linux/Unix system supported. Libraries needed are :

* 	Wigxjpf (http://fy.chalmers.se/subatom/wigxjpf/) to compute 3j,6j and 9j symbols. You should download the program and store it in the main directory of the library. Unmount the zip in the main directory of EPRL library, enter the folder and make it. We use version 1.7 of this code.

*	Gmp (https://gmplib.org/) to compute d-small Wigner matrices for coherent states and for booster function. We need high precision operations.

* 	Mpfr (http://www.mpfr.org). As GMP, high precision library for floating points. Used in coherent states and boosters computations.

* 	Mpc (http://www.multiprecision.org/mpc/). As GMP and MPFR, high precision library for complex numbers. Used for booster functions.

* 	Gsl https://www.gnu.org/software/gsl/doc/html/intro.html to compute complex gamma functions in booster functions.

* 	libconfig (http://www.hyperrealm.com/libconfig/) to write and use configuration files.

* 	OpenMp http://www.openmp.org/ to parallelize the computation.

To build the library and compile the main programs you can use the make file:

* `make lib` will build the library

* `make amplitude` will compile the main programs

* `make ` will do both

To link the library use `-lSL2C`.

### Licenses

sl2cfoam and so sl2cfoam-asym are free softwares: you can redistribute them and/or modify them under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

sl2cfoam is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

If you use the library you should cite the paper in which we describe the library.

### How to use it

We assign ten boundary spins, they are the “areas” of the faces of five tetrahedra glued together to form a 4 dimensional 4-simplex.
We then rescale them using a parameter N. We assign boundary angles ( 𝜃,  𝛷 ) parametrizing tetrahedra’s normals and the Immirzi parameter 𝛾.
All boundary informations should be given in the configuration file SL2CConfigurations.txt as in the examples.

You can ran the whole program from the bash file SL2CBash in the following way:

./SL2CBash conf 2*N1 2*N2 2*𝛥LMax α

where conf is the name of the configuration in the config file (for instance EquilateralConfig), N1 and N2 determines the range for the rescaling parameter N, 𝛥LMax is the homogeneous cut off maximum on the infinite sum of magnetic spins l and α is related to the number of standard deviation of boundary coherent states that you want to consider while summing over combinations of intertwiners (0 represents all the intertwiner's range).
For instance, to compute the SL2C 4-simplex amplitude for an equilateral configuration from spins 2 to spin 4  up to the first shell we use

./SL2C Bash EquilateralConfig 4 8 2 0

If you want to reach the convergence on the l's summations, at j=2, you can run it as

./SL2C Bash EquilateralConfig 2 2 20 0

In this particular case the program will reach convergence at LMax=2 and will stop.

The 𝜎 acts as a cutoff on boundary intertwiners. From spin 2*j=10 the error between the true value and the value with α = 3 is order 3%. It is a very good approximation and the program becomes way faster.

For details about the main program, about needed packages and tests read below and https://arxiv.org/abs/1807.03066

### Headers and Functions

*	CoherentStatesFunctions.h in inc contains definitions for d-small Wigner functions and for coherent states at fixed intertwines
* 	khash.h to create hash tables. It has been modified from https://github.com/attractivechaos/klib/blob/master/khash.h and from https://github.com/	attractivechaos/klib/issues/44 in order to accomodate array as keys and double complex as values. Write function has been taken from https://		github.com/attractivechaos/klib/pull/76/commits/494da2ec77e4059bf3a5ca8561a1763e2a6bc2ad.
* 	J6Canonicalization.h (modified from wigxjpf code) to assign, given a certain combination of entries, a “key” to a 6j symbol.
* 	B4Hash.h (modified from Collet’s code). Our B4 functions are computed starting from Collet’s formula and using his code modified from C++ to C.
* 	Utilities.h . Various useful functions.

### Source Programs

* Coherent States:

Creation of coherent states hash tables for 15j symbol computations.

The program computes five coherent state for the five boundary tetrahedra storing data in the hash table.
The key are {1,iA} for the first tetrahedra, {2,iB} for the second and so on. All intertwines iA,iB.. are computed.
Neither Coherent States nor j6 symbols depend on the Immirzi parameter.
They are saved in the folder data/ as two_j1.two_j2.two_j3.two_j4.two_j5.two_j6.two_j7.two_j8.two_j9.two_j10.coh. We use the notation two_j=2*j in order to accomodate half-integers and to follow the same conventions of wigxjpf code.

* J6Hash:

Creation of 6j hash tables for 15j symbol computations.
As before, J6Hash saves data as two_j1.two_j2.two_j3.two_j4.two_j5.two_j6.two_j7.two_j8.two_j9.two_j10_two_Dl.6j.

* B4Hash:

Creation of Booster functions hash tables for 15j symbol computations.

Booster functions depends on Immirzi parameter so data are saved in data/Immirzi𝛾/HashTableBooster/.
File are as two_j1.two_j2.two_j3.two_j4.two_j5.two_j6.two_j7.two_j8.two_j9.two_j10_two_Dl.boost.

* SL2CSymbol:

Creation of the EPRL SL(2,C) vertex for a single 4-simplex.

To use SL2CSymbol we must have already created Hash Tables for both coherent states, 6j symbols and Booster Functions.
Values are saved in txt in data/Immirzi𝛾/SL2CResults/Alpha_α/ in four columns.
On the first two columns there are real and imaginary part of the SL2C symbol for given boundary spin while on the last two columns there are real and imaginary part of the re-phased value via saddle point (this should be real and the imaginary part is a control variable.)

We also save the value of the symbol for each set of l spins in data/Immirzi𝛾/HashTablesSL2C/Alpha_α/ in order to avoid doing multiple times the same computation.

### To Do

We index the files throught the spins but to equal sets of spins we could assign different boundary data. For instance with all equal spins we could assign normal for an Euclidean 4-simplex, but also for a vector geometry or for a non geometrical configuration. To avoid problems we reccomend to create a folder with the code for Euclidean boundary data, one for Lorentzian and so on.  We are working to implement a better indexing to avoid this problem.

